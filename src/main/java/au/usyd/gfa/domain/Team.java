package au.usyd.gfa.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

@Entity
@Table(name="team")

public class Team implements Serializable{
	@Id
	@GeneratedValue
	@Column(name="id")
	private long id;
	
	@Column(name="capacity")
	private int capacity;
	
	@Column(name="current_number")
	private int currentNumber;
	
	@Column(name="course_id")
	private long courseId;
	
	@Column(name="description")
	private String description;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	public int getCurrentNumber() {
		return currentNumber;
	}
	
	public void setCurrentNumber(int currentNumber) {
		this.currentNumber = currentNumber;
	}
	
	public long getCourseId() {
		return courseId;
	}
	
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
