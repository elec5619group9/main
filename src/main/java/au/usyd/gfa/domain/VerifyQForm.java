package au.usyd.gfa.domain;

/**
 * The class for the form in the Verify Security Questions' part, being used to retrieve information in the form
 * And also set the constraints to those input fields
 * 
 * @author ulricawu
 * */
import javax.validation.constraints.NotEmpty;

public class VerifyQForm {
	@NotEmpty(message="Your Student Number must not be empty!")
	private String verifySid;
	
	@NotEmpty(message="Security Answer 1 must not be empty!")
	private String verifyA1;
	
	@NotEmpty(message="Security Answer 2 must not be empty!")
	private String verifyA2;
	
	public void setVerifySid(String verifySid) {
		this.verifySid = verifySid;
	}
	public String getVerifySid() {
		return verifySid;
	}
	
	public void setVerifyA1(String verifyA1) {
		this.verifyA1 = verifyA1;
	}
	public String getVerifyA1() {
		return verifyA1;
	}
	
	public void setVerifyA2(String verifyA2) {
		this.verifyA2 = verifyA2;
	}
	public String getVerifyA2() {
		return verifyA2;
	}
}