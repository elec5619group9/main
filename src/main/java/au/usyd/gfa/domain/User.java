package au.usyd.gfa.domain;

/**
 * The entity of User with some string fields and specify the corresponding table in the database.
 * And also specify the Getter and Setter methods for those string fields.
 * 
 *  @author ulricawu
 * */
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

@Entity
@Table(name="user")
public class User implements Serializable {
	
	@Id
	@Column(name="sid")
	private long sid;
	
	@Column(name="email_address")
	private String emailAddress;

	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="password")
	private String password;
	
	@Column(name="security_answer1")
	private String securityAnswer1;
	
	@Column(name="security_answer2")
	private String securityAnswer2;
	
	@Column(name="team_id")
	private long teamId;
	
	@Column(name="is_leader")
	private boolean isLeader;
	
	@Column(name="profile_picture")
	private String profilePicture = "default.png";
	
	@Column(name="self_intro")
	private String selfIntro;
	
	
	public long getSid() {
		return sid;
	}
	
	public void setSid(long sid) {
		this.sid = sid;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}
	
	public void setEmailAddress(String newEmailAddress) {
		this.emailAddress = newEmailAddress;
	}
	
	public String getfirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getlastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getSecurityAnswer1() {
		return securityAnswer1;
	}
	
	public void setSecurityAnswer1(String securityAnswer1) {
		this.securityAnswer1 = securityAnswer1;
	}
	
	public String getSecurityAnswer2() {
		return securityAnswer2;
	}
	
	public void setSecurityAnswer2(String securityAnswer2) {
		this.securityAnswer2 = securityAnswer2;
	}
	
	public long getTeamId() {
		return teamId;
	}
	
	public void setTeamId(long teamId) {
		this.teamId = teamId;
	}
	
	public boolean getIsLeader() {
		return isLeader;
	}
	
	public void setIsLeader(boolean isLeader) {
		this.isLeader = isLeader;
	}
	
	public String getProfilePicture() {
		return profilePicture;
	}
	
	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}
	
	public String getSelfIntro() {
		return selfIntro;
		
	}
	
	public void setSelfIntro(String selfIntro) {
		this.selfIntro = selfIntro;
	}
	
}
