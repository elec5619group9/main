package au.usyd.gfa.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

@Entity
@Table(name="team_owned_tag")

public class OwnedTag implements Serializable{
	@Id
	@GeneratedValue
	@Column(name="id")
	private long id;
	
	@Column(name="team_id")
	private long teamId;
	
	@Column(name="tag_id")
	private long tagId;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getTeamId() {
		return teamId;
	}
	
	public void setTeamId(long teamId) {
		this.teamId = teamId;
	}
	
	public long getTagId() {
		return tagId;
	}
	
	public void setTagId(long tagId) {
		this.tagId = tagId;
	}
	
	
}
