package au.usyd.gfa.domain;

/**
 * The class for the form in the Register part, being used to retrieve information in the form
 * And also set the constraints to those input fields
 * 
 * @author ulricawu
 * */
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class RegistForm {
	private String registerSid;
	
	@NotEmpty(message="Email Address must be not empty!")
	@Email(message="Please follow the email address format!")
	private String registerEmail;
	
	@NotEmpty(message="Password must not be empty!")
	@Pattern(regexp="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()])(?=\\S+$).{5,}$", message="Not match format!")
	private String registerPwd;
	
	@NotEmpty(message="Please confirm your password!")
	private String checkPwd;
	
	@NotEmpty(message="Security Answer 1 must not be empty!")
	private String registAnswer1;
	
	@NotEmpty(message="Security Answer 2 must not be empty!")
	private String registAnswer2;
	
	public void setRegisterSid(String registSid) {
		this.registerSid = registSid;
	}
	public String getRegisterSid() {
		return registerSid;
	}
	
	public void setRegisterEmail(String registEml) {
		this.registerEmail = registEml;
	}
	public String getRegisterEmail() {
		return registerEmail;
	}
	
	public void setRegisterPwd(String registPwd) {
		this.registerPwd = registPwd;
	}
	public String getRegisterPwd() {
		return registerPwd;
	}
	
	public void setCheckPwd(String confirmPwd) {
		this.checkPwd = confirmPwd;
	}
	public String getCheckPwd() {
		return checkPwd;
	}
	
	public void setRegistAnswer1(String registAnswer1) {
		this.registAnswer1 = registAnswer1;
	}
	public String getRegistAnswer1() {
		return registAnswer1;
	}
	
	public void setRegistAnswer2(String registAnswer2) {
		this.registAnswer2 = registAnswer2;
	}
	public String getRegistAnswer2() {
		return registAnswer2;
	}
}
