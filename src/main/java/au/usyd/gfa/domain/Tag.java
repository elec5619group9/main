package au.usyd.gfa.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

@Entity
@Table(name="tag")

public class Tag implements Serializable{
	@Id
	@GeneratedValue
	@Column(name="id")
	private long id;
	
	@Column(name="tag_name")
	private String tagName;
	
	@Column(name="tag_color")
	private String tagColor;
	
	@Column(name="course_id")
	private long courseId;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getTagName() {
		return tagName;
	}
	
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	
	public String getTagColor() {
		return tagColor;
	}
	
	public void setTagColor(String tagColor) {
		this.tagColor = tagColor;
	}
	
	public long getCourseId() {
		return courseId;
	}
	
	public void setCourseId(long courseId) {
		this.courseId = courseId;
	}
	
	
}
