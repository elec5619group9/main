package au.usyd.gfa.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

@Entity
@Table(name="request")

public class Request implements Serializable{
	@Id
	@GeneratedValue
	@Column(name="id")
	private long id;
	
	@Column(name="sender_id")
	private long senderId;
	
	@Column(name="receiver_id")
	private long receiverId;
	
	@Column(name="sender_current_number")
	private long senderCurrentNumber;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getSenderId() {
		return senderId;
	}
	
	public void setSenderId(long senderId) {
		this.senderId = senderId;
	}
	
	public long getReceiverId() {
		return receiverId;
	}
	
	public void setReceiverId(long receiverId) {
		this.receiverId = receiverId;
	}
	
	public long getSenderCurrentNumber() {
		return senderCurrentNumber;
	}
	
	public void setSenderCurrentNumber(long senderCurrentNumber) {
		this.senderCurrentNumber = senderCurrentNumber;
	}
	
}
