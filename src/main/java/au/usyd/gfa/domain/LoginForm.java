package au.usyd.gfa.domain;

/**
 * The class for the form in the Login part, being used to retrieve information in the form
 * And also set the constraints to those input fields
 * 
 * @author ulricawu
 * */
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class LoginForm {
	@NotEmpty(message="Student ID must not be empty!")
	private String loginSid;
	
	@NotEmpty(message="Password must be be empty!")
	private String loginPwd;

	public void setLoginSid(String loginSid) {
		this.loginSid = loginSid;
	}
	public String getLoginSid() {
		return loginSid;
	}
	public void setLoginPwd(String password) {
		this.loginPwd = password;
	}
	public String getLoginPwd() {
		return loginPwd;
	}
}

