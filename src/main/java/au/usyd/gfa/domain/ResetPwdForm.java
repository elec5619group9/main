package au.usyd.gfa.domain;

/**
 * The class for the form in the ResetPwd part, being used to retrieve information in the form
 * And also set the constraints to those input fields
 * 
 * @author ulricawu
 * */
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class ResetPwdForm {
	@NotEmpty(message="Password must not be empty!")
	@Pattern(regexp="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()])(?=\\S+$).{5,}$", message="Not match format!")
	private String resetPwd;
	
	@NotEmpty(message="Please confirm your password!")
	private String confirmPwd;
	
	public void setResetPwd(String resetPwd) {
		this.resetPwd = resetPwd;
	}
	public String getResetPwd() {
		return resetPwd;
	}
	
	public void setConfirmPwd(String confirmPwd) {
		this.confirmPwd = confirmPwd;
	}
	public String getConfirmPwd() {
		return confirmPwd;
	}
}