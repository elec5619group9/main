package au.usyd.gfa.service;

/**
 * An interface of the Authentication Service for handling business logic or rules received and before
 * passing to the DAO layer
 * 
 * @author ulricawu
 * */
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import au.usyd.gfa.domain.User;

public interface AuthenticationService {
	
	public void checkLoginInfo(String loginSid, String loginPwd, Errors errors);
	
	public void addUser(User user, Errors errors);
	
	public void checkVerifyAnswer(String verifySid, String answer1, String answer2, Errors errors);
	
	public User getUser(long sid);
	
	public void updateUser(String userSid, String newPassword, Errors errors);
	
	public void validateRegister(Object target, Errors errors);	
	
	public void validateReset(Object target, Errors errors);
	
}