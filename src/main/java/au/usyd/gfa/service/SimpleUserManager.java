package au.usyd.gfa.service;

import java.util.*;

import au.usyd.gfa.domain.User;

public class SimpleUserManager {
	private List<User> users;
		
	public void setUsersByTeam(List<User> users) {
		this.users = users;
	}
		
	public List<User> getUsersByTeam(Long Sid){
		List<User> result = new ArrayList<User>();
		for(int i = 0; i < users.size(); i++) {
			if(users.get(i).getSid() == Sid) {
				result.add(users.get(i));
			}
		}
		return result;
	}
}