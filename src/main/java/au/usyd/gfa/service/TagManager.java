package au.usyd.gfa.service;
import java.util.*;

import au.usyd.gfa.domain.Tag;
import au.usyd.gfa.domain.Team;
import au.usyd.gfa.domain.User;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value="tagManager")
@Transactional
public class TagManager {
	private SessionFactory sessionFactory;
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public Tag getTagById(long id) {
		return (Tag) sessionFactory.getCurrentSession().get(Tag.class, id);
	}
	
	public List<Tag> getAllTags(){
		Session currentSession = this.sessionFactory.getCurrentSession();
		List<Tag> tags = currentSession.createQuery("FROM Tag").list();
		return tags;
	}
	
	


}
