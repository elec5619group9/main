package au.usyd.gfa.service;
import java.util.*;

import au.usyd.gfa.domain.ExpectedTag;
import au.usyd.gfa.domain.OwnedTag;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value="ownedTagManager")
@Transactional
public class OwnedTagManager{
	private SessionFactory sessionFactory;
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public List<OwnedTag> getTagsById(long teamId){
		Session currentSession = this.sessionFactory.getCurrentSession();
		List<OwnedTag> ownedTags = (List<OwnedTag>)currentSession.createQuery("FROM OwnedTag ET WHERE ET.teamId = :teamId").setParameter("teamId", teamId).list();
		return ownedTags;
	}
	
	public void updateOwnedTag(OwnedTag ownedTag) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(ownedTag);
	}
	
	public void deleteOwnedTag(OwnedTag ownedTag) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.delete(ownedTag);
	}
	
	public void addOwnedTag(OwnedTag ownedTag) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.save(ownedTag);
	}
	

}
