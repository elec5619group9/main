package au.usyd.gfa.service;
import java.util.*;


import au.usyd.gfa.domain.OwnedTag;
public class SimpleOwnedTagManager {
	private List<OwnedTag> ownedTags;
	
	public void setTags(List<OwnedTag> tags) {
		this.ownedTags = tags;
	}
	
	public List<OwnedTag> getOwnedTags(Long Id){
		List<OwnedTag> tags = new ArrayList<OwnedTag>();
		for(int i = 0; i < ownedTags.size(); i++) {
			if(ownedTags.get(i).getTeamId() == Id) {
				tags.add(ownedTags.get(i));
			}
		}
		return tags;
	}
	
	public void addOwnedTag(OwnedTag ownedTag) {
		ownedTags.add(ownedTag);
	}
	
	public void deleteOwnedTag(OwnedTag ownedTag) {
			ownedTags.remove(ownedTag);
	}
	
	public void updateOwnedTag(OwnedTag ownedTag) {
		for(int i =0;i<ownedTags.size();i++) {
			if(ownedTags.get(i).getTagId()==ownedTag.getTagId()&&
					ownedTags.get(i).getTeamId()==ownedTag.getTeamId()) {

			}else {
				ownedTags.add(ownedTag);
				break;
			}
			
		}
	}
	
	
	
	
}
