package au.usyd.gfa.service;
import java.util.*;

import au.usyd.gfa.domain.ExpectedTag;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value="expectedTagManager")
@Transactional
public class ExpectedTagManager{
	private SessionFactory sessionFactory;
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public List<ExpectedTag> getTagsById(long teamId){
		Session currentSession = this.sessionFactory.getCurrentSession();
		List<ExpectedTag> expectedTags = (List<ExpectedTag>)currentSession.createQuery("FROM ExpectedTag ET WHERE ET.teamId = :teamId").setParameter("teamId", teamId).list();
		return expectedTags;
	}
	
	public void updateExpectedTag(ExpectedTag expectedTag) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(expectedTag);
	}
	
	public void deleteExpectedTag(ExpectedTag expectedTag) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.delete(expectedTag);
	}
	
	public void addExpectedTag(ExpectedTag expectedTag) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.save(expectedTag);
	}
	
	

}
