package au.usyd.gfa.service;
import java.util.*;

import au.usyd.gfa.domain.Request;
import au.usyd.gfa.domain.User;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value="requestManager")
@Transactional
public class RequestManager{
	private SessionFactory sessionFactory;
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public boolean addRequest(Long senderId, Long receiverId, Long currentNumber) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Request request = (Request)currentSession.createQuery("FROM Request R WHERE R.senderId = :senderId AND R.receiverId = :receiverId").setParameter("senderId", senderId).setParameter("receiverId", receiverId).uniqueResult();
		if(request!=null) {
			return false;
		}else {
			request = new Request();
			request.setSenderId(senderId);
			request.setReceiverId(receiverId);
			request.setSenderCurrentNumber(currentNumber);
			this.sessionFactory.getCurrentSession().save(request);
			return true;
		}	
	}
	
	public List<Request> getRequests(Long myId){
		Session currentSession = this.sessionFactory.getCurrentSession();
		List<Request> requests = currentSession.createQuery("FROM Request R WHERE R.receiverId = :myId").setParameter("myId", myId).list();
		return requests;
	}
	
	public Request getRequestById(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Request request = (Request) currentSession.get(Request.class, id);
		return request;
	}
	
	public List<Request> getRequestBySender(Long senderId) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		List<Request> requests = currentSession.createQuery("FROM Request R WHERE R.senderId = :senderId").setParameter("senderId", senderId).list();
		List<Request> r = currentSession.createQuery("FROM Request R WHERE R.receiverId = :senderId").setParameter("senderId", senderId).list();
		for(int i = 0; i < r.size(); i++) {
			requests.add(r.get(i));
		}
		return requests;
	}
	
	public int getRequestNumber(Long myId) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		List<Request> requests = currentSession.createQuery("FROM Request R WHERE R.receiverId = :myId").setParameter("myId", myId).list();
		return requests.size();
	}
	
	
	public void deleteRequest(Request request) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.delete(request);
	}

	
	
	

}