package au.usyd.gfa.service;

/**
 * The implementation of the Authentication Service interface for handling business logic or rules received and before
 * passing to the DAO layer
 * 
 * @author ulricawu
 * */
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import au.usyd.gfa.dao.UserDAO;
import au.usyd.gfa.domain.RegistForm;
import au.usyd.gfa.domain.ResetPwdForm;
import au.usyd.gfa.domain.User;

@Service("AuthenticationService")
public class AuthenServiceImple implements AuthenticationService {

	private final Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private UserDAO userDAO;

	public void setLoginDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	
	/**
	 * Check the information that user has input to see if the account has existed or if the password is wrong
	 * Pass the detailed operations to the DAO layer with the parameters.
	 *  
	 * @param loginSid the student number
	 * @param loginPwd the login password
	 * @param errors errors handler
	 * */
	@Override
	public void checkLoginInfo(String loginSid, String loginPwd, Errors errors) {
		log.info("In Service class....Check Login");
		userDAO.checkLoginInfo(loginSid, loginPwd, errors);
	}

	/**
	 * Pass the detailed operations to the DAO layer with the parameters
	 * in order to add new user information
	 * 
	 * @param user the new user object
	 * @param errors the errors handler
	 * */
	@Override
	public void addUser(User user, Errors errors) {		
		userDAO.addUser(user, errors);
	}

	/**
	 * Pass the detailed operations to the DAO layer with the parameters
	 * to retrieve the user information with the specific student id number
	 * 
	 * @param sid the student id(unique identifier)
	 * 
	 * @return User the found user object with the search sid
	 * */
	@Override
	public User getUser(long sid) {
		return userDAO.getUser(sid);
	}

	/**
	 * Pass the detailed operations to the DAO layer with the parameters
	 * to update the user instance in the database with the updated password
	 * 
	 * @param userSid the unique student number to identify the student to be updated
	 * @param newPwd the new password for updating
	 * @param errors errors handler
	 * */
	@Override
	public void updateUser(String userSid, String newPwd, Errors errors) {
		userDAO.updateUser(userSid, newPwd, errors);
	}

	/**
	 * Validate the information input in the register form to check if the input against constraints
	 * 
	 * @param target the specific form
	 * @param errors the errors handler
	 * */
	@Override
	public void validateRegister(Object target, Errors errors) {
		RegistForm registForm = (RegistForm) target;
	    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "registerSid",
	        "NotEmpty.registForm.registerSid",
	        "Registration Student ID must not be Empty.");
	   
	    if (!(registForm.getRegisterPwd()).equals(registForm.getCheckPwd())) {
	      errors.rejectValue("registerPwd",
	          "matchingPassword.registForm.registerPwd",
	          "Password and Confirm Password Not match.");
	    }
		
	}

	/**
	 * Pass the detailed operations to the DAO layer with the parameters
	 * to check the security answer to see if the answers match with the answers stored
	 * 
	 * @param verifySid the unique id number to identify the specific student
	 * @param answer1 the security answer1
	 * @param answer2 the security answer2
	 * */
	@Override
	public void checkVerifyAnswer(String verifySid, String answer1, String answer2, Errors errors) {
		userDAO.checkVerifyAnswer(verifySid, answer1, answer2, errors);		
	}

	/**
	 * Validate the information input in the resetPwd form to check if two passwords equals
	 * 
	 * @param target the spefici resetPwdForm
	 * @param errors errors handler
	 * */
	@Override
	public void validateReset(Object target, Errors errors) {
		ResetPwdForm resetPwdForm = (ResetPwdForm) target;
		log.info("resetting password: "+resetPwdForm.getResetPwd());
		if (!(resetPwdForm.getResetPwd()).equals(resetPwdForm.getConfirmPwd())) {
			//log.info("hhhhhh"+resetPwdForm.getResetPwd());
			errors.rejectValue("resetPwd",
			          "matchingPassword.resetPwdForm.resetPwd",
			          "Password and Confirm Password Not match.");
		}
		
	}
}
