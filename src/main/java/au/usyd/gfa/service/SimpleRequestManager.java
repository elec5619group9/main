package au.usyd.gfa.service;

import java.util.*;

import au.usyd.gfa.domain.Request;

public class SimpleRequestManager {
	private List<Request> requests;
	
	public void setRequests(List<Request> requests) {
		this.requests = requests;
	}
	
	public List<Request> getRequests(Long myId){
		List<Request> result = new ArrayList<Request>();
		for(int i = 0; i < requests.size(); i++) {
			if(requests.get(i).getReceiverId() == myId) {
				result.add(requests.get(i));
			}
		}
		return result;
	}
	
	public boolean addRequest(Long senderId, Long receiverId, Long currentNumber) {
		Request r = new Request();
		r.setSenderId(senderId);
		r.setReceiverId(receiverId);
		r.setSenderCurrentNumber(currentNumber);
		for(int i = 0; i < requests.size(); i++) {
			if(requests.get(i).getReceiverId() == receiverId && requests.get(i).getSenderId() == senderId) {
				
				return false;
			}
		}
		requests.add(r);
		return true;
	}
	
	public int getRequestNumber(Long myId) {
		List<Request> result = new ArrayList<Request>();
		for(int i = 0; i < requests.size(); i++) {
			if(requests.get(i).getReceiverId() == myId) {
				result.add(requests.get(i));
			}
		}
		return result.size();
	}
	
	

}
