package au.usyd.gfa.service;
import java.util.*;


import au.usyd.gfa.domain.Team;
public class SimpleTeamManager {
	private List<Team> teams = new ArrayList<Team>();
	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}
	public List<Team> getGroups(){
		
		return teams;
	}
	public Team getGroupById(long id) {
		for(Team t: teams)
		{
			if(t.getId()== id )
			{
				return t;
			}
		}
		return null;
	}
	public boolean checkGroupExist(long id) {
		if(!teams.isEmpty())
		for(Team t: teams)
		{
			if(t.getId()== id )
			{
				return true;
			}
		}
		return false;
	}
	public void updateGroup(Team team) {
		for(int i=0;i<teams.size();i++)
		{
			if(teams.get(i).getId()== team.getId() )
			{
				teams.set(i, team);
				return;
			}
		}
	}
	public void deleteGroup(Team team) {
		for(int i=0;i<teams.size();i++)
		{
			if(teams.get(i).getId()== team.getId() )
			{
				teams.remove(i);
			}
		}
	}
	public void addGroup(Team team) {
		teams.add(team);
	}
}
