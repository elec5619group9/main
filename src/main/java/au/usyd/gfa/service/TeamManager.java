package au.usyd.gfa.service;
import java.util.*;
import au.usyd.gfa.domain.User;
import au.usyd.gfa.domain.Team;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value="teamManager")
@Transactional
public class TeamManager{
	private SessionFactory sessionFactory;
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public List<Team> getGroups(){
		Session currentSession = this.sessionFactory.getCurrentSession();
		List<Team> teams = currentSession.createQuery("FROM Team").list();
		return teams;
	}
	
	public Team getGroupById(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Team team = (Team) currentSession.get(Team.class, id);
		return team;
	}
	
	public boolean checkGroupExist(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Team team = (Team) currentSession.get(Team.class, id);
		if(team != null) {
			return true;
		}
		return false;	
	}
	
	public void updateGroup(Team team) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(team);
	}
	
	public void deleteGroup(Team team) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.delete(team);
	}
	
	public void addGroup(Team team) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.save(team);
	}
	
	

}
