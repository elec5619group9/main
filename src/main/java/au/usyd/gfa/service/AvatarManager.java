package au.usyd.gfa.service;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

@Service(value="avatarManager")
public class AvatarManager {


    public static String saveFile(MultipartFile uploadFile, HttpServletRequest request) {
        String pathval = request.getSession().getServletContext().getRealPath("/")+"resources/";
        // Obtain the image path 
        String newFileName = String.valueOf( System.currentTimeMillis());
        String saveFilePath = "images/uploadFile";
        /* build file directory */
        File fileDir = new File(pathval + saveFilePath);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        //upload name
        String filename=uploadFile.getOriginalFilename();
        //extention name
        String extensionName = filename.substring(filename.lastIndexOf(".") + 1);
        try {
            String imgPath = saveFilePath + "/"+ filename;
            FileOutputStream out = new FileOutputStream(pathval + imgPath);
//            // write in file
            out.write(uploadFile.getBytes());
            out.flush();
            out.close();
            return imgPath;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}