package au.usyd.gfa.service;
import java.util.*;

import au.usyd.gfa.domain.Request;
import au.usyd.gfa.domain.ExpectedTag;
public class SimpleExpectedTagManager {
	private List<ExpectedTag> expectedTags;
	
	public void setTags(List<ExpectedTag> tags) {
		this.expectedTags = tags;
	}
	
	public List<ExpectedTag> getExpectedTags(Long Id){
		List<ExpectedTag> tags = new ArrayList<ExpectedTag>();
		for(int i = 0; i < expectedTags.size(); i++) {
			if(expectedTags.get(i).getTeamId() == Id) {
				tags.add(expectedTags.get(i));
			}
		}
		return tags;
	}
	
	public void addExpectedTag(ExpectedTag expectedTag) {
		expectedTags.add(expectedTag);
	}
	
	public void deleteExpectedTag(ExpectedTag expectedTag) {
			expectedTags.remove(expectedTag);
	}
	
	public void updateExpectedTag(ExpectedTag expectedTag) {
		for(int i =0;i<expectedTags.size();i++) {
			if(expectedTags.get(i).getTagId()==expectedTag.getTagId()&&
					expectedTags.get(i).getTeamId()==expectedTag.getTeamId()) {

			}else {
				expectedTags.add(expectedTag);
				break;
			}
			
		}
	}
	
	
	
	
}
