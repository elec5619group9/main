package au.usyd.gfa.service;
import java.util.*;

import au.usyd.gfa.dao.UserDAO;
import au.usyd.gfa.domain.User;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value="userManager")
@Transactional
public class UserManager{
	private SessionFactory sessionFactory;
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	private UserDAO userDAO;

	public void setLoginDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	

	public User getUser(long sid) {
		return userDAO.getUser(sid);
	}
	
	
	public List<User> getUsersByTeam(long teamId){
		Session currentSession = this.sessionFactory.getCurrentSession();
		List<User> users = currentSession.createQuery("FROM User U WHERE U.teamId = :teamId").setParameter("teamId", teamId).list();
		return users;
	}
	
	public void updateUser(User user) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(user);
	}
	

}
