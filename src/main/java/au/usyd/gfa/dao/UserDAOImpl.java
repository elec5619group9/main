package au.usyd.gfa.dao;

/**
 * The actual implementation of the UserDAO interface, and it overrides those functions
 * 
 * @author ulricawu
 * */
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;

import au.usyd.gfa.domain.User;

@Repository("UserDAO")
@Transactional
public class UserDAOImpl implements UserDAO {
	
	private final Log log = LogFactory.getLog(getClass());
	public BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);
	
	@Resource(name="sessionFactory")
    protected SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
	}

	protected Session getSession(){
        return sessionFactory.openSession();
	}
	
	/**
	 * Check the information that user has input to see if the account has existed or if the password is wrong 
	 * @param loginSid the student number
	 * @param loginPwd the login password
	 * @param errors errors handler
	 * */
	@Override
	public void checkLoginInfo(String loginSid, String loginPwd, Errors errors) {
		log.info("In Check login....");

		if(!errors.hasErrors()) {
			if(getUser(Long.parseLong(loginSid)) == null) {
				errors.rejectValue("loginSid","noFoundAccount");
			} else {
				User usr = getUser(Long.parseLong(loginSid));
				if(!encoder.matches(loginPwd, usr.getPassword())) {
					errors.rejectValue("loginPwd","wrongLoginPwd");
				}
			}
		}
		//return resultCode;
	}

	/**
	 * Add new user to the database if the register information is correct
	 * Else give prompts
	 * 
	 * @param user the new user object
	 * @param errors the errors handler
	 * */
	@Override
	public void addUser(User user, Errors errors) {
		if(getUser(user.getSid()) != null) {
			errors.rejectValue("registerSid", "duplicateRegistSid");
		} else {
			sessionFactory.getCurrentSession().save(user);
		}
		
	}

	/**
	 * Retrieve the user information with the specific student id number
	 * @param sid the student id(unique identifier)
	 * 
	 * @return User the found user object with the search sid
	 * */
	@Override
	public User getUser(long sid) {
		return (User) sessionFactory.getCurrentSession().get(
                User.class, sid);
	}

	/**
	 * Update the user instance in the database with the updated password
	 * @param userSid the unique student number to identify the student to be updated
	 * @param newPwd the new password for updating
	 * @param errors errors handler
	 * */
	@Override
	public void updateUser(String userSid, String newPwd, Errors errors) {
		User user = getUser(Long.parseLong(userSid));
		user.setPassword(newPwd);
		if(user!=null) {
			sessionFactory.getCurrentSession().update(user);
		}
		errors.rejectValue("resetPwd", "wrongUpdate");
        //return user;
	}

	/**
	 * Check the security answer to see if the answers match with the answers stored
	 * @param verifySid the unique id number to identify the specific student
	 * @param answer1 the security answer1
	 * @param answer2 the security answer2
	 * */
	@Override
	public void checkVerifyAnswer(String verifySid, String answer1, String answer2, Errors errors) {
		if(getUser(Long.parseLong(verifySid))!=null) {
			User u = getUser(Long.parseLong(verifySid));
			if(!u.getSecurityAnswer1().equals(answer1)) {
				errors.rejectValue("verifyA1", "wrongVerifyAnswer");
			}
			if(!u.getSecurityAnswer2().equals(answer2)) {
				errors.rejectValue("verifyA2", "wrongVerifyAnswer");
			}
		} else {
			errors.rejectValue("verifySid", "noFoundAccount");
		}
		
	}

}