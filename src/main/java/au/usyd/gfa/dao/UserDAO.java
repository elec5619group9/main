package au.usyd.gfa.dao;

import org.springframework.validation.Errors;

import au.usyd.gfa.domain.User;

/**
 * An interface of the UserDAO for handling any operations on user domain in the database
 * 
 * @author ulricawu
 * */
public interface UserDAO {
	
	public void checkLoginInfo(String loginSid, String loginPwd, Errors errors);
	
	public void addUser(User user, Errors errors);
	
	public void checkVerifyAnswer(String verifySid, String answer1, String answer2, Errors errors);
	
	public User getUser(long sid);
	
	public void updateUser(String usersid, String newPwd, Errors errors);

}