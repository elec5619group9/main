package au.usyd.gfa.web;
import java.io.IOException;
import java.util.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.http.ResponseEntity;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import com.google.gson.Gson;
import com.pusher.rest.Pusher;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;

import au.usyd.gfa.domain.Request;
import au.usyd.gfa.domain.Team;
import au.usyd.gfa.domain.User;
import au.usyd.gfa.service.AuthenticationService;
import au.usyd.gfa.service.RequestManager;
import au.usyd.gfa.service.UserManager;

@Controller
public class RequestController {
	protected final Log logger = LogFactory.getLog(getClass());
	Pusher pusher = new Pusher("1093020", "8472d4f313fe13532815", "3026334c0674745139b1");
	
	@Resource(name="requestManager")
	private RequestManager requestManager;
	@Resource(name="userManager")
	private UserManager userManager;
	
	@RequestMapping(value = "/request", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Void> sendRequest(HttpServletRequest request) {
		try {
			//receive json
			String jsonString = IOUtils.toString(request.getInputStream());
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<HashMap<String, Long>> typeRef 
            = new TypeReference<HashMap<String, Long>>() {};
			HashMap<String, Long> map = mapper.readValue(jsonString, typeRef);
			

			if(requestManager.addRequest(map.get("sender_id"), map.get("receiver_id"), map.get("current_number"))) {
				//puhser
				pusher.setCluster("ap3");
				pusher.setEncrypted(true);
				
				//get users from receiver team
				List<User> receivers = userManager.getUsersByTeam(map.get("receiver_id"));
				for(int i = 0; i < receivers.size(); i++) {
					//send message to each receiver's channel
					pusher.trigger("request-channel" + receivers.get(i).getSid(), "merge-request-event", Collections.singletonMap("message", "You have received a merge request from team " + map.get("sender_id")));
				}
				
				return new ResponseEntity<Void>(HttpStatus.CREATED);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	}
	
	@RequestMapping(value ="/request", method = RequestMethod.GET)
	public String showRequest(Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		Object sid = session.getAttribute("userSid");
		if(sid == null) {
			return "redirect:/404";
		}
		User user = userManager.getUser(Long.parseLong((String)sid));
		List<Request> requests = requestManager.getRequests(user.getTeamId());
		model.addAttribute("user", user);
		model.addAttribute("requests", requests);
		model.addAttribute("isLeader", user.getIsLeader());
		model.addAttribute("firstName", user.getfirstName());
		return "request";
	}

}
