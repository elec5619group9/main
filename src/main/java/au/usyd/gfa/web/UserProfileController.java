package au.usyd.gfa.web;

import java.util.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import au.usyd.gfa.domain.ExpectedTag;
import au.usyd.gfa.domain.OwnedTag;
import au.usyd.gfa.domain.Tag;
import au.usyd.gfa.domain.Team;
import au.usyd.gfa.domain.User;
import au.usyd.gfa.service.AuthenticationService;
import au.usyd.gfa.service.AvatarManager;
import au.usyd.gfa.service.ExpectedTagManager;
import au.usyd.gfa.service.OwnedTagManager;
import au.usyd.gfa.service.RequestManager;
import au.usyd.gfa.service.TagManager;
import au.usyd.gfa.service.TeamManager;
import au.usyd.gfa.service.UserManager;

@Controller
public class UserProfileController {
	protected final Log logger = LogFactory.getLog(getClass());

	@Resource(name = "userManager")
	private UserManager userManager;
	@Resource(name = "teamManager")
	private TeamManager teamManager;
	@Resource(name = "expectedTagManager")
	private ExpectedTagManager expectedTagManager;
	@Resource(name = "ownedTagManager")
	private OwnedTagManager ownedTagManager;
	@Resource(name = "requestManager")
	private RequestManager requestManager;
	@Resource(name = "tagManager")
	private TagManager tagManager;
	@Resource(name = "AuthenticationService")
	private AuthenticationService authenticationService;
	@Resource(name = "avatarManager")
	private AvatarManager avatarManager;
	@RequestMapping(value = "/userProfile", method = RequestMethod.GET)
	
	public String showUserProfile(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		Object sid = session.getAttribute("userSid");
		User user = userManager.getUser(Long.parseLong((String) sid));
		if (sid == null) {
			return "redirect:/404";
		}
		Team myTeam = teamManager.getGroupById(user.getTeamId());
		int numOfRequests = requestManager.getRequestNumber(myTeam.getId());
		model.addAttribute("user", user);
		model.addAttribute("firstName", user.getfirstName());
		model.addAttribute("isLeader", user.getIsLeader());
		model.addAttribute("numOfRequests", numOfRequests);
		return "userProfile";
	}
	
	@RequestMapping(value="/updateProfile", method = RequestMethod.GET)
	public String showUpdateProfile(Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		Object sid = session.getAttribute("userSid");
		User user = userManager.getUser(Long.parseLong((String) sid));
		if(sid == null) {
			return "redirect:/404";
		}
		Team myTeam = teamManager.getGroupById(user.getTeamId());
		int numOfRequests = requestManager.getRequestNumber(myTeam.getId());
		model.addAttribute("user", user);
		model.addAttribute("numOfRequests", numOfRequests);
		model.addAttribute("firstName", user.getfirstName());
		model.addAttribute("isLeader", user.getIsLeader());
		return "updateProfile";
	}
	
	@RequestMapping(value="/updateProfile", method=RequestMethod.POST)
	public String updateProfile(HttpServletRequest request, @RequestParam("avatar") MultipartFile file, Model model) {
	 	HttpSession session = request.getSession();
		Object sid = session.getAttribute("userSid");
		User user = userManager.getUser(Long.parseLong((String) sid));
		
//		if(!file.isEmpty()) {
//			String img = avatarManager.saveFile(file, request);
//			user.setProfilePicture(img);
//		}
		String profilePicture = request.getParameter("profile_picture");
		String firstName = request.getParameter("firstname");
		String lastName = request.getParameter("lastname");
		String intro = request.getParameter("intro");
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setSelfIntro(intro);
		user.setProfilePicture(profilePicture);
		userManager.updateUser(user);
		return "redirect:/userProfile";
	}
}