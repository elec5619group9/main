package au.usyd.gfa.web;

import java.io.File;
import java.io.IOException;
import java.util.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.http.ResponseEntity;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.RequestBody;
import com.google.gson.Gson;
import com.pusher.rest.Pusher;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;

import au.usyd.gfa.domain.ExpectedTag;
import au.usyd.gfa.domain.LoginForm;
import au.usyd.gfa.domain.OwnedTag;
import au.usyd.gfa.domain.Request;
import au.usyd.gfa.domain.Tag;
import au.usyd.gfa.domain.Team;
import au.usyd.gfa.domain.User;
import au.usyd.gfa.service.AuthenticationService;
import au.usyd.gfa.service.AvatarManager;
import au.usyd.gfa.service.ExpectedTagManager;
import au.usyd.gfa.service.OwnedTagManager;
import au.usyd.gfa.service.RequestManager;
import au.usyd.gfa.service.TagManager;
import au.usyd.gfa.service.TeamManager;
import au.usyd.gfa.service.UserManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.view.RedirectView;




@Controller
public class ProfileCreationController {
	
	@Resource(name="teamManager")
	private TeamManager teamManager;
	
	@Resource(name="userManager")
	private UserManager userManager;
	
	@Resource(name="tagManager")
	private TagManager tagManager;
	
	@Resource(name="ownedTagManager")
	private OwnedTagManager ownedTagManager;
	
	@Resource(name="expectedTagManager")
	private ExpectedTagManager expectedTagManager;
	
	@Resource(name="avatarManager")
	private AvatarManager avatarManager;
	
	 @RequestMapping(value="/createProfile", method = RequestMethod.GET)
		public String showCreateProfile(Model model, HttpServletRequest request) {
			HttpSession session = request.getSession();
			Object sid = session.getAttribute("userSid");
			if(sid == null) {
				return "redirect:/404";
			}
			return "createProfile";
		}
	 @RequestMapping(value="/createProfile", method=RequestMethod.POST)
		public String createProfile(HttpServletRequest request, @RequestParam("avatar") MultipartFile file, Model model) {
		 	HttpSession session = request.getSession();
			Object sid = session.getAttribute("userSid");
			User user = userManager.getUser(Long.parseLong((String) sid));
			//upload img to cloudniary api
//			if(!file.isEmpty()) {
//				String img = avatarManager.saveFile(file, request);
//				user.setProfilePicture(img);
//				
//			}
			String profilePicture = request.getParameter("profile_picture");
			String firstName = request.getParameter("firstname");
			String lastName = request.getParameter("lastname");
			String intro = request.getParameter("intro");
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setSelfIntro(intro);
			user.setProfilePicture(profilePicture);
			userManager.updateUser(user);
			return "redirect:/createTeam";
		}
}
	 
