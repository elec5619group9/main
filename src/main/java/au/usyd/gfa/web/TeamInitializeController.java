package au.usyd.gfa.web;

import java.io.IOException;
import java.util.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.http.ResponseEntity;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import com.google.gson.Gson;
import com.pusher.rest.Pusher;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;

import au.usyd.gfa.domain.ExpectedTag;
import au.usyd.gfa.domain.LoginForm;
import au.usyd.gfa.domain.OwnedTag;
import au.usyd.gfa.domain.Request;
import au.usyd.gfa.domain.Tag;
import au.usyd.gfa.domain.Team;
import au.usyd.gfa.domain.User;
import au.usyd.gfa.service.AuthenticationService;
import au.usyd.gfa.service.ExpectedTagManager;
import au.usyd.gfa.service.OwnedTagManager;
import au.usyd.gfa.service.RequestManager;
import au.usyd.gfa.service.TagManager;
import au.usyd.gfa.service.TeamManager;
import au.usyd.gfa.service.UserManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.view.RedirectView;




@Controller
public class TeamInitializeController {
	
	@Resource(name="teamManager")
	private TeamManager teamManager;
	
	@Resource(name="userManager")
	private UserManager userManager;
	
	@Resource(name="tagManager")
	private TagManager tagManager;
	
	@Resource(name="ownedTagManager")
	private OwnedTagManager ownedTagManager;
	
	@Resource(name="expectedTagManager")
	private ExpectedTagManager expectedTagManager;
	
	 @RequestMapping(value="/createTeam", method = RequestMethod.GET)
		public String showCreateTeam(Model model, HttpServletRequest request) {
			HttpSession session = request.getSession();
			Object sid = session.getAttribute("userSid");
			if(sid == null) {
				return "redirect:/404";
			}
			//get all tags
			List<Tag> allTags = tagManager.getAllTags();
			model.addAttribute("allTags", allTags);
			return "createTeam";
		}
	 @RequestMapping(value="/createTeam", method=RequestMethod.POST)
		public String createTeam(HttpServletRequest request, Model model) {
		 	HttpSession session = request.getSession();
			Object sid = session.getAttribute("userSid");
			User user = userManager.getUser(Long.parseLong((String) sid));
			
			long courseId = Long.parseLong(request.getParameter("course"));
			int capacity = Integer.parseInt(request.getParameter("capacity"));
			int numOfTags = tagManager.getAllTags().size();
			String description = request.getParameter("description");
			String[] expectedTags = new String[numOfTags];
			String[] ownedTags = new String[numOfTags];
			for(int i = 0; i < numOfTags; i++) {
				expectedTags[i] = request.getParameter("expectedTag" + i);
				ownedTags[i] = request.getParameter("ownedTag" + i);
			}
			Team newTeam = new Team();
			newTeam.setCapacity(capacity);
			newTeam.setCurrentNumber(1);
			newTeam.setCourseId(courseId);
			newTeam.setDescription(description);
			teamManager.addGroup(newTeam);
			user.setTeamId(newTeam.getId());
			user.setIsLeader(true);
			userManager.updateUser(user);
			for(int i = 0; i < numOfTags; i++) {
				if(expectedTags[i]!=null) {
					ExpectedTag tag = new ExpectedTag();
					tag.setTagId(i+1);
					tag.setTeamId(newTeam.getId());
					expectedTagManager.addExpectedTag(tag);
				}
				
				if(ownedTags[i]!=null) {
					OwnedTag tag = new OwnedTag();
					tag.setTagId(i+1);
					tag.setTeamId(newTeam.getId());
					ownedTagManager.addOwnedTag(tag);
				}
			}
			return "redirect:/pool";
		}
}
	 
