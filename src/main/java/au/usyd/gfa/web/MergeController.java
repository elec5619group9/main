package au.usyd.gfa.web;
import java.io.IOException;
import java.util.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.http.ResponseEntity;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;
import com.google.gson.Gson;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;

import au.usyd.gfa.domain.ExpectedTag;
import au.usyd.gfa.domain.OwnedTag;
import au.usyd.gfa.domain.Request;
import au.usyd.gfa.domain.Team;
import au.usyd.gfa.domain.User;
import au.usyd.gfa.service.ExpectedTagManager;
import au.usyd.gfa.service.OwnedTagManager;
import au.usyd.gfa.service.RequestManager;
import au.usyd.gfa.service.TeamManager;
import au.usyd.gfa.service.UserManager;

@Controller
public class MergeController {
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Resource(name="requestManager")
	private RequestManager requestManager;
	@Resource(name="teamManager")
	private TeamManager teamManager;
	@Resource(name="expectedTagManager")
	private ExpectedTagManager expectedTagManager;
	@Resource(name="ownedTagManager")
	private OwnedTagManager ownedTagManager;
	@Resource(name="userManager")
	private UserManager userManager;

	@RequestMapping(value = "/merge", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> merge(HttpServletRequest request) {
		try {
			
			String jsonString = IOUtils.toString(request.getInputStream());
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<HashMap<String, Long>> typeRef 
            = new TypeReference<HashMap<String, Long>>() {};
			HashMap<String, Long> map = mapper.readValue(jsonString, typeRef);
			//check sender team exists
			if(teamManager.checkGroupExist(map.get("sender_id"))) {
				Team senderTeam = teamManager.getGroupById(map.get("sender_id"));
				Team myTeam = teamManager.getGroupById(map.get("receiver_id"));
				int senderCurrentNumber = senderTeam.getCurrentNumber();
				int myCurrentNumber = myTeam.getCurrentNumber();
				int myCapacity = myTeam.getCapacity();
				if(senderCurrentNumber+myCurrentNumber <= myCapacity) {
					//add members
					myTeam.setCurrentNumber(senderCurrentNumber+myCurrentNumber);
					teamManager.updateGroup(myTeam);
					//merge expected tag
					List<ExpectedTag> senderETags = expectedTagManager.getTagsById(senderTeam.getId());
					List<ExpectedTag> myETags = expectedTagManager.getTagsById(myTeam.getId());
					HashSet<Long> eSet = new HashSet<Long>();
					for(int i = 0; i < myETags.size(); i++) {
						eSet.add(myETags.get(i).getTagId());
					}
					for(int i = 0; i < senderETags.size(); i++) {
						if(eSet.add(senderETags.get(i).getTagId())) {
							senderETags.get(i).setTeamId(myTeam.getId());
							expectedTagManager.updateExpectedTag(senderETags.get(i));
						}else {
							expectedTagManager.deleteExpectedTag(senderETags.get(i));
						}
					}
					//
					//merge expected tag
					List<OwnedTag> senderOTags = ownedTagManager.getTagsById(senderTeam.getId());
					List<OwnedTag> myOTags = ownedTagManager.getTagsById(myTeam.getId());
					HashSet<Long> oSet = new HashSet<Long>();
					for(int i = 0; i < myOTags.size(); i++) {
						eSet.add(myOTags.get(i).getTagId());
					}
					for(int i = 0; i < senderOTags.size(); i++) {
						if(eSet.add(senderOTags.get(i).getTagId())) {
							senderOTags.get(i).setTeamId(myTeam.getId());
							ownedTagManager.updateOwnedTag(senderOTags.get(i));
						}else {
							ownedTagManager.deleteOwnedTag(senderOTags.get(i));
						}
					}
					//merge users
					List<User> senderUsers = userManager.getUsersByTeam(senderTeam.getId());
					for(int i = 0; i < senderUsers.size(); i++) {
						senderUsers.get(i).setTeamId(myTeam.getId());
						senderUsers.get(i).setIsLeader(false);
						userManager.updateUser(senderUsers.get(i));
					}
					//delete request with sender
					List<Request> senderRequests = requestManager.getRequestBySender(senderTeam.getId());
					for(int i = 0; i < senderRequests.size(); i++) {
						requestManager.deleteRequest(senderRequests.get(i));
					}
					//delete senderTeam
					teamManager.deleteGroup(senderTeam);
					return new ResponseEntity<String>(HttpStatus.OK);
					
				}else {
					return new ResponseEntity<String>("Over capacity", HttpStatus.CONFLICT);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity<String>("Sender team doesn't exist", HttpStatus.CONFLICT);
	}
	
	@RequestMapping(value = "/reject", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> reject(HttpServletRequest request) {
		try {
			
			String jsonString = IOUtils.toString(request.getInputStream());
			ObjectMapper mapper = new ObjectMapper();
			TypeReference<HashMap<String, Long>> typeRef 
            = new TypeReference<HashMap<String, Long>>() {};
			HashMap<String, Long> map = mapper.readValue(jsonString, typeRef);
			Request r = requestManager.getRequestById(map.get("request_id"));
			requestManager.deleteRequest(r);
			
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity<String>(HttpStatus.OK);
	}
}
