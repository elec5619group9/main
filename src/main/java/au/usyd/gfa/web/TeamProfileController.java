package au.usyd.gfa.web;

import java.util.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import au.usyd.gfa.domain.ExpectedTag;
import au.usyd.gfa.domain.OwnedTag;
import au.usyd.gfa.domain.Tag;
import au.usyd.gfa.domain.Team;
import au.usyd.gfa.domain.User;
import au.usyd.gfa.service.AuthenticationService;
import au.usyd.gfa.service.AvatarManager;
import au.usyd.gfa.service.ExpectedTagManager;
import au.usyd.gfa.service.OwnedTagManager;
import au.usyd.gfa.service.RequestManager;
import au.usyd.gfa.service.TagManager;
import au.usyd.gfa.service.TeamManager;
import au.usyd.gfa.service.UserManager;

@Controller
public class TeamProfileController {
	protected final Log logger = LogFactory.getLog(getClass());

	@Resource(name = "userManager")
	private UserManager userManager;
	@Resource(name = "teamManager")
	private TeamManager teamManager;
	@Resource(name = "expectedTagManager")
	private ExpectedTagManager expectedTagManager;
	@Resource(name = "ownedTagManager")
	private OwnedTagManager ownedTagManager;
	@Resource(name = "requestManager")
	private RequestManager requestManager;
	@Resource(name = "tagManager")
	private TagManager tagManager;
	@Resource(name = "AuthenticationService")
	private AuthenticationService authenticationService;
	@Resource(name = "avatarManager")
	private AvatarManager avatarManager;
	
	@RequestMapping(value = "/teamProfile", method = RequestMethod.GET)
	public String showTeamProfile(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		Object sid = session.getAttribute("userSid");
		User user = userManager.getUser(Long.parseLong((String) sid));
		if (sid == null) {
			return "redirect:/404";
		}
		Team myTeam = teamManager.getGroupById(user.getTeamId());
		//my expected tags
		List<ExpectedTag> expectedTags = expectedTagManager.getTagsById(myTeam.getId());
		List<Tag> myExpectedTags = new ArrayList<Tag>();
		for(int i = 0; i < expectedTags.size(); i++) {
			Tag tag = tagManager.getTagById(expectedTags.get(i).getTagId());
			myExpectedTags.add(tag);
			
		}
		//my owned tags
		List<OwnedTag> ownedTags = ownedTagManager.getTagsById(myTeam.getId());
		List<Tag> myOwnedTags = new ArrayList<Tag>();
		for(int i = 0; i < ownedTags.size(); i++) {
			Tag tag = tagManager.getTagById(ownedTags.get(i).getTagId());
			myOwnedTags.add(tag);	
		}
		List<User> users = userManager.getUsersByTeam(myTeam.getId());
		int numOfRequests = requestManager.getRequestNumber(myTeam.getId());
		model.addAttribute("user", user);
		model.addAttribute("users", users);
		model.addAttribute("myTeam", myTeam);
		model.addAttribute("firstName", user.getfirstName());
		model.addAttribute("isLeader", user.getIsLeader());
		model.addAttribute("expectedTags", myExpectedTags);
		model.addAttribute("ownedTags", myOwnedTags);
		model.addAttribute("numOfRequests", numOfRequests);
		return "teamProfile";
	}
	
	@RequestMapping(value="/updateTeamProfile", method = RequestMethod.GET)
	public String showUpdateTeamProfile(Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		Object sid = session.getAttribute("userSid");
		User user = userManager.getUser(Long.parseLong((String) sid));
		if(sid == null) {
			return "redirect:/404";
		}
		Team myTeam = teamManager.getGroupById(user.getTeamId());
		List<User> users = userManager.getUsersByTeam(myTeam.getId());
		int numOfRequests = requestManager.getRequestNumber(myTeam.getId());
		model.addAttribute("user", user);
		model.addAttribute("users", users);
		model.addAttribute("team", myTeam);
		model.addAttribute("numOfRequests", numOfRequests);
		model.addAttribute("firstName", user.getfirstName());
		model.addAttribute("isLeader", user.getIsLeader());
		return "updateTeamProfile";
	}
	
	@RequestMapping(value="/updateTeamProfile", method=RequestMethod.POST)
	public String updateTeamProfile(HttpServletRequest request, Model model) {
	 	HttpSession session = request.getSession();
		Object sid = session.getAttribute("userSid");
		User user = userManager.getUser(Long.parseLong((String) sid));
		Team myTeam = teamManager.getGroupById(user.getTeamId());
		int capacity = Integer.parseInt(request.getParameter("capacity"));
		String description = request.getParameter("description");
		long newLeader = Long.parseLong(request.getParameter("leader"));
		user.setIsLeader(false);
		userManager.updateUser(user);
		User leader = userManager.getUser(newLeader);
		leader.setIsLeader(true);
		userManager.updateUser(leader);
		myTeam.setCapacity(capacity);
		myTeam.setDescription(description);
		teamManager.updateGroup(myTeam);
		return "redirect:/teamProfile";
	}
}