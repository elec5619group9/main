package au.usyd.gfa.web;

import java.util.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import au.usyd.gfa.domain.ExpectedTag;
import au.usyd.gfa.domain.OwnedTag;
import au.usyd.gfa.domain.Tag;
import au.usyd.gfa.domain.Team;
import au.usyd.gfa.domain.User;
import au.usyd.gfa.service.AuthenticationService;
import au.usyd.gfa.service.AvatarManager;
import au.usyd.gfa.service.ExpectedTagManager;
import au.usyd.gfa.service.OwnedTagManager;
import au.usyd.gfa.service.RequestManager;
import au.usyd.gfa.service.TagManager;
import au.usyd.gfa.service.TeamManager;
import au.usyd.gfa.service.UserManager;

@Controller
public class PoolController {

	protected final Log logger = LogFactory.getLog(getClass());

	@Resource(name = "userManager")
	private UserManager userManager;
	@Resource(name = "teamManager")
	private TeamManager teamManager;
	@Resource(name = "expectedTagManager")
	private ExpectedTagManager expectedTagManager;
	@Resource(name = "ownedTagManager")
	private OwnedTagManager ownedTagManager;
	@Resource(name = "requestManager")
	private RequestManager requestManager;
	@Resource(name = "tagManager")
	private TagManager tagManager;
	@Resource(name = "AuthenticationService")
	private AuthenticationService authenticationService;
	@Resource(name = "avatarManager")
	private AvatarManager avatarManager;
	

	@RequestMapping(value = "/pool", method = RequestMethod.GET)
	public String showPool(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		Object sid = session.getAttribute("userSid");
		if (sid == null) {
			return "redirect:/404";
		}
		User user = userManager.getUser(Long.parseLong((String) sid));
		Team myTeam = teamManager.getGroupById(user.getTeamId());
		//my expected tags
		List<ExpectedTag> expectedTags = expectedTagManager.getTagsById(myTeam.getId());
		List<Tag> myExpectedTags = new ArrayList<Tag>();
		for(int i = 0; i < expectedTags.size(); i++) {
			Tag tag = tagManager.getTagById(expectedTags.get(i).getTagId());
			myExpectedTags.add(tag);
			
		}
		//my owned tags
		List<OwnedTag> ownedTags = ownedTagManager.getTagsById(myTeam.getId());
		List<Tag> myOwnedTags = new ArrayList<Tag>();
		for(int i = 0; i < ownedTags.size(); i++) {
			Tag tag = tagManager.getTagById(ownedTags.get(i).getTagId());
			myOwnedTags.add(tag);
			
			
		}
		List<Team> teams = teamManager.getGroups();
		ListIterator<Team> iterator = teams.listIterator();
		int numOfRequests = requestManager.getRequestNumber(myTeam.getId());
		while (iterator.hasNext()) {
			Team team = iterator.next();
			if (team.getId() == myTeam.getId()) {
				iterator.remove();
			}
		}

		// get teams' tags
		List<List<Tag>> listOfExpectedTags = new ArrayList<List<Tag>>();
		List<List<Tag>> listOfOwnedTags = new ArrayList<List<Tag>>();
		List<List<User>> listOfUsers = new ArrayList<List<User>>();
		for (int i = 0; i < teams.size(); i++) {
			//team i tags
			List<ExpectedTag> team_expectedTags = expectedTagManager.getTagsById(teams.get(i).getId());
			List<Tag> team_i_expectedTags = new ArrayList<Tag>();
			for(int j = 0; j < team_expectedTags.size(); j++) {
				Tag tag = tagManager.getTagById(team_expectedTags.get(j).getTagId());
				team_i_expectedTags.add(tag);
			}
			listOfExpectedTags.add(team_i_expectedTags);
			
			List<OwnedTag> team_ownedTags = ownedTagManager.getTagsById(teams.get(i).getId());
			List<Tag> team_i_ownedTags = new ArrayList<Tag>();
			for(int j = 0; j < team_ownedTags.size(); j++) {
				Tag tag = tagManager.getTagById(team_ownedTags.get(j).getTagId());
				team_i_ownedTags.add(tag);
			}
			listOfOwnedTags.add(team_i_ownedTags);
			
			//get users in team i
			List<User> users = userManager.getUsersByTeam(teams.get(i).getId());
			listOfUsers.add(users);
		}
		//get all tags
		List<Tag> allTags = tagManager.getAllTags();
		
		
		model.addAttribute("user", user);
		model.addAttribute("teams", teams);
		model.addAttribute("noOfTeams", teams.size());
		model.addAttribute("listOfExpectedTags", listOfExpectedTags);
		model.addAttribute("listOfOwnedTags", listOfOwnedTags);
		model.addAttribute("listOfUsers", listOfUsers);
		model.addAttribute("sid", user.getSid());
		model.addAttribute("firstName", user.getfirstName());
		model.addAttribute("isLeader", user.getIsLeader());
		model.addAttribute("myTeam", myTeam);
		model.addAttribute("expectedTags", myExpectedTags);
		model.addAttribute("ownedTags", myOwnedTags);
		model.addAttribute("numOfRequests", numOfRequests);
		model.addAttribute("allTags", allTags);
		return "pool";
	}

	@RequestMapping(value = "/404", method = RequestMethod.GET)
	public String show404(Model model) {
		return "404";
	}
	
	
	@RequestMapping(value="/otherProfile/{sid}", method=RequestMethod.GET)
	public String showOtherProfile(HttpServletRequest request, @PathVariable("sid") Long sid, Model model) {
		HttpSession session = request.getSession();
		Object user_sid = session.getAttribute("userSid");
		User user = userManager.getUser(Long.parseLong((String) user_sid));
		if (sid == null) {
			return "redirect:/404";
		}
		Team myTeam = teamManager.getGroupById(user.getTeamId());
		int numOfRequests = requestManager.getRequestNumber(myTeam.getId());
		User other = userManager.getUser(sid);
		model.addAttribute("user", user);
		model.addAttribute("other", other);
		model.addAttribute("firstName", user.getfirstName());
		model.addAttribute("isLeader", user.getIsLeader());
		model.addAttribute("numOfRequests", numOfRequests);
		return "otherProfile";
	}

}
