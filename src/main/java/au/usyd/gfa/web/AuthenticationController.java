package au.usyd.gfa.web;

/**
 * The Controller for handling any requests related to the Authentication part, like logining, registering and so on
 * 
 * @author ulricawu
 * */
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import au.usyd.gfa.domain.LoginForm;
import au.usyd.gfa.domain.RegistForm;
import au.usyd.gfa.domain.ResetPwdForm;
import au.usyd.gfa.domain.User;
import au.usyd.gfa.domain.VerifyQForm;
import au.usyd.gfa.service.AuthenticationService;
import au.usyd.gfa.service.UserManager;

@Controller
public class AuthenticationController{
	protected final Log logger = LogFactory.getLog(getClass());
	@Resource(name="userManager")
	private UserManager userManager;
	
	@Autowired
	public AuthenticationService loginService;
	
	public BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);
	
	/**
	 * Display the default/first page of the application
	 * 
	 * @return the login view
	 * */
	@RequestMapping(value="/",  method=RequestMethod.GET)
	public String showForm(Map model) {
		LoginForm loginForm = new LoginForm();
		model.put("loginForm", loginForm);
		return "login";
	}	
	
	/**
	 * Map the POST request in the login page once user has submitted input information for login
	 * and use the service to check the input information
	 * 
	 * @param loginForm the form containing the input information
	 * @param result BindingResult for the current form
	 * 
	 * @return different views for different situations 
	 * */
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String login(HttpServletRequest httpServletRequest, @Valid LoginForm loginForm, BindingResult result,
			Model model) {
		loginService.checkLoginInfo(loginForm.getLoginSid(), loginForm.getLoginPwd(), result);
		if(result.hasErrors()) {
			return "login";
		} else {
			httpServletRequest.getSession().setAttribute("userSid", loginForm.getLoginSid());
			User user = userManager.getUser(Long.parseLong(loginForm.getLoginSid()));
			if(user.getfirstName() == null) {
				return "redirect:/createProfile";
			}
			if(user.getTeamId()==0) {
				return "redirect:/createTeam";
			}
			//model.addAttribute("userSid", loginForm.getLoginSid());
			return "redirect:/pool";
		}
		
	}
	
	/**
	 * Map the GET request in the register page
	 * 
	 * @return register view
	 * */
	@RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView registration(Model model) {
		ModelAndView v = new ModelAndView("register");
        return v;
    }
	
	/**
	 * Map the POST request in the register page once user has submitted input information for registeration
	 * and use the service to validate input information or add new users
	 * 
	 * @param registForm the form containing the input information
	 * @param result BindingResult for the current form
	 * 
	 * @return different views for different situations
	 * */
	@RequestMapping(value="/register", method=RequestMethod.POST)
    public String addUser(HttpServletRequest httpServletRequest, @Valid RegistForm registForm, BindingResult result
    		, Model model) {
		loginService.validateRegister(registForm, result);
		if (result.hasErrors()) {
			return "register";
		} else {
			User newUser = new User();
			newUser.setSid(Long.parseLong(registForm.getRegisterSid()));
			newUser.setEmailAddress(registForm.getRegisterEmail());
			String password = encoder.encode(registForm.getRegisterPwd());
			newUser.setPassword(password);
			newUser.setSecurityAnswer1(registForm.getRegistAnswer1());
			newUser.setSecurityAnswer2(registForm.getRegistAnswer2());
			loginService.addUser(newUser, result);
			if(result.hasErrors()) {
				return "register";
			} else {
				return "redirect:/";
			}
		}
	}
 	
	/**
	 * Map the GET request in the verify Security Questions page
	 * 
	 * @return the verify Security Question's view
	 * */
	@RequestMapping(value = "/verifySecurityQ", method = RequestMethod.GET)
    public String verifySecurityQ(Model model) {
        return "verifySecurityQ";
    }
	
	/**
	 * Map the POST request in the verify Security Question page once user has submitted input information
	 * for verify Security Answers and use the service to validate input information
	 * 
	 * @return different views for different situations
	 * */
	@RequestMapping(value = "/verifySecurityQ", method = RequestMethod.POST)
    public String submitVerifyAnswers(HttpServletRequest httpServletRequest, @Valid VerifyQForm verifyQForm, BindingResult result
    		, Model model) {
		if(result.hasErrors()) {
			return "verifySecurityQ";
		} else {
			loginService.checkVerifyAnswer(verifyQForm.getVerifySid(), verifyQForm.getVerifyA1(), verifyQForm.getVerifyA2(), result);
			if(result.hasErrors()) {
				return "verifySecurityQ";
			} else {
				httpServletRequest.getSession().setAttribute("changePwdSid", verifyQForm.getVerifySid());
				return "redirect:/resetPwd";
			}
		}
    }
	
	/**
	 * Map the GET request in the resetPwd page
	 * 
	 * @return the resetPwd view
	 * */
	@RequestMapping(value = "/resetPwd", method = RequestMethod.GET)
    public String resetPwdPage(HttpServletRequest httpServletRequest, Model model) {
		//logger.info("Change Password for "+httpServletRequest.getSession().getAttribute("changePwdSid"));
		String changePwdSid = (String) httpServletRequest.getSession().getAttribute("changePwdSid");
        return "resetPwd";
    }
	
	/**
	 * Map the POST request in the resetPwd page once user has submitted input information
	 * for resetting password and use the service to validate input information
	 * 
	 * @return different views for different situations
	 * */
	@RequestMapping(value = "/resetPwd", method = RequestMethod.POST)
	public String resetPwd(HttpServletRequest httpServletRequest, @Valid ResetPwdForm resetPwdForm, 
			BindingResult result){
		logger.info("New Password: "+resetPwdForm.getResetPwd());
		loginService.validateReset(resetPwdForm, result);
		if(result.hasErrors()) {
			return "resetPwd";
		} else {
			if(httpServletRequest.getSession() != null) {
				String newPwd = encoder.encode(resetPwdForm.getResetPwd());
				loginService.updateUser((String)httpServletRequest.getSession().getAttribute("changePwdSid"), 
						newPwd, result);
				HttpSession httpSession = httpServletRequest.getSession();
				if(httpSession != null) {
					httpSession.invalidate();
				}
				return "redirect:/";
			} else {
				result.rejectValue("SidError", "noLoginUser");
				return "resetPwd";
			}
		}
	}
	
	/**
	 * Map the request for logout
	 * If user loged out, then invalidate the current session and return the default page
	 * 
	 * @return the default login page
	 * */
	@RequestMapping(value="/logout")
	public String logout(HttpServletRequest httpServletRequest) {
		HttpSession httpSession = httpServletRequest.getSession();
		if(httpSession != null) {
			httpSession.invalidate();
		}
	    return "redirect:/";
	}
}