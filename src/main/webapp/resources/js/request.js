$(document).ready(function() {
	console.log("Hi!");
});
function mergeTeam(senderId, receiverId) {
	var data = {};
	data["sender_id"] = senderId;
	data["receiver_id"] = receiverId;
	axios.post('/gfa/merge', data)
		.then(response => {
			console.log(response.status);
			var modelId = "teamModal" + senderId;
			$("#" + modelId).modal('hide');
			$("#success").show();
			let countDownDate = new Date().getTime()+6000;
			setInterval(function() {
				let now = new Date().getTime();
				let distance = Math.floor((countDownDate - now) / 1000);
				console.log(distance);
				document.getElementById("success").innerText = "You will be redirected to Team pool " + distance + "s."
			}, 1000);
			setTimeout(function() {
				window.location.href="/gfa/pool";
			}, 5000);
		})
		.catch(error => {
			console.log(error.response.status);
			console.log(error.response.data);
			var modelId = "teamModal" + senderId;
			$("#" + modelId).modal('hide');
			if (error.response.data === "Over capacity") {
				$("#overCap").show();
				setTimeout(function() {
					$('#OverCap').hide()
				}, 4000);
			} else {
				$('noTeam').show();
				setTimeout(function() {
					$('#noTeam').hide()
				}, 4000);
			}
		});
}

function hideAlert() {
	$(".alert").hide();
}

function rejectTeam(requestId){
	var data = {};
	data["request_id"] = requestId;
	axios.post('/gfa/reject', data)
		.then(response => {
			console.log(response.status)
			$("#" + requestId).hide();
			})
			.catch(error =>{
			console.log(error.status)
			});
			
}