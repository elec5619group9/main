var CLOUDINARY_URL = "	https://api.cloudinary.com/v1_1/dmw6kgqfq/upload";
var CLOUDINARY_UPLOAD_PRESET = "ocawmmje";

var imgPreview = $("#show-avatar")
var fileUpload = $("#avatar")


fileUpload.change(function (event){
	imgPreview.attr("src", "resources/images/spin.gif");
	var file = event.target.files[0];
	var formData = new FormData();
	formData.append('file', file);
	formData.append('upload_preset', CLOUDINARY_UPLOAD_PRESET);
	axios({
		url:CLOUDINARY_URL,
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		data: formData
	}).then(function(response){
		imgPreview.attr("src", response.data.secure_url);
		$("#profile_picture").val(response.data.secure_url);
	}).catch(function(error){
		console.error(error)
	});
	
});