$(document).ready(function() {
	$(".leader-radio").change(function() {
		$(".leader-radio").each(function() {
			if ($(this).is(':checked')) {
				$("#cap" + $(this).val()).show()
			} else {
				$("#cap" + $(this).val()).hide()
			}
		});
	});
});

function mincap() {
	var x = document.getElementById("teamcap").value;
	if (x > 1) { x = x - 1; }
	else
		alert("needs to be greater than 1")
	document.getElementById("teamcap").value = x;
}

function pluscap() {
	var x = document.getElementById("teamcap").value;
	x++;
	document.getElementById("teamcap").value = x;
}
