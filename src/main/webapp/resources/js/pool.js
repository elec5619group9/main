$(document).ready(function() {
	//search function
	$("#search").on("keyup", function() {
		var value = $(this).val().toLowerCase();
		$(".other-teams-info").hide()
		$(".other-teams-info *").filter(function() {
			if ($(this).text().toLowerCase().indexOf(value) > -1) {
				$(this).parent(".other-teams-info").show()
			}
		});
		if (!$(this).val()) {
			$(".other-teams-info").show()
		}
	});
	//expected skills filter
	var checkedExpectedArray = [];
	var checkedOwnedArray = [];
	$(".expected-checks").change(function() {
		if ($(this).is(':checked')) {
			checkedExpectedArray.push($(this).val())
			$(this).parent().removeClass('badge-light')
			for (i = 0; i < noOfTeams; i++) {
				var expectedArray = []
				var ownedArray = []
				$("#expected-tags" + i + " *").filter(function() {
					expectedArray.push($(this).text())
				})
				$("#owned-tags" + i + " *").filter(function() {
					ownedArray.push($(this).text())
				})
				var include = true
				if(checkedExpectedArray.length != 0){
					include = checkedExpectedArray.every(val => expectedArray.includes(val))
				}
				if(checkedOwnedArray.length != 0){
					include = include && checkedOwnedArray.every(val => ownedArray.includes(val))
				}
				console.log(include)
				if(include === false){
					$("#team"+i).hide()
				}
			}
		} else {
			$(this).parent().addClass('badge-light')
			var index = checkedExpectedArray.indexOf($(this).val())
			checkedExpectedArray.splice(index, 1)
			for (i = 0; i < noOfTeams; i++) {
				var expectedArray = []
				var ownedArray = []
				$("#expected-tags" + i + " *").filter(function() {
					expectedArray.push($(this).text())
				})
				$("#owned-tags" + i + " *").filter(function() {
					ownedArray.push($(this).text())
				})
				var include = true
				if(checkedExpectedArray.length != 0){
					include = checkedExpectedArray.every(val => expectedArray.includes(val))
				}
				if(checkedOwnedArray.length!=0){
					include = include && checkedOwnedArray.every(val => ownedArray.includes(val))
				}
				console.log(include)
				if(include === true){
					$("#team"+i).show()
				}
			}

		}
	})
	//owned skills filter
	$(".owned-checks").change(function() {
		if ($(this).is(':checked')) {
			checkedOwnedArray.push($(this).val())
			$(this).parent().removeClass('badge-light')
			for (i = 0; i < noOfTeams; i++) {
				var expectedArray = []
				var ownedArray = []
				$("#owned-tags" + i + " *").filter(function() {
					ownedArray.push($(this).text())
					console.log($(this).text())
				})
				$("#expected-tags" + i + " *").filter(function() {
					expectedArray.push($(this).text())
				})
				var include = true
				if(checkedOwnedArray.length!=0){
				include = checkedOwnedArray.every(val => ownedArray.includes(val))
				}
				if(checkedExpectedArray.length!=0){
					include = include && checkedExpectedArray.every(val => expectedArray.includes(val))
				}
				console.log(include)
				if(include === false){
					$("#team"+i).hide()
				}
			}
		} else {
			$(this).parent().addClass('badge-light')
			var index = checkedOwnedArray.indexOf($(this).val())
			checkedOwnedArray.splice(index, 1)
			for (i = 0; i < noOfTeams; i++) {
				var expectedArray = []
				var ownedArray = []
				$("#owned-tags" + i + " *").filter(function() {
					ownedArray.push($(this).text())
				})
				$("#expected-tags" + i + " *").filter(function() {
					expectedArray.push($(this).text())
				})
				var include = true
				if(checkedOwnedArray.length!=0){
					include = checkedOwnedArray.every(val => ownedArray.includes(val))
				}
				if(checkedExpectedArray.length!=0){
					include = include && checkedExpectedArray.every(val => expectedArray.includes(val))
				}
				console.log(include)
				if(include === true){
					$("#team"+i).show()
				}
			}

		}
	})
	
});
function sendRequest(senderId, receiverId, currentNumber) {
	var data = {};
	data["sender_id"] = senderId;
	data["receiver_id"] = receiverId;
	data["current_number"] = currentNumber;
	axios.post('/gfa/request', data)
		.then(response => {
			console.log(response.status);
			var modelId = "teamModal" + receiverId;
			$("#" + modelId).modal('hide');
			$('#success').show();
			setTimeout(function() {
				$('#success').hide()
			}, 4000);
		})
		.catch(error => {
			console.log(error.response.status);
			var modelId = "teamModal" + receiverId;
			$("#" + modelId).modal('hide');
			$("#warning").show();
			setTimeout(function() {
				$('#warning').hide();
			}, 4000);
		});
}


function hideAlert() {
	$(".alert").hide();
}

//pusher setting
var pusher = new Pusher('8472d4f313fe13532815', {
	cluster: 'ap3'
});

//subscribe to specific channel
var channel = pusher.subscribe('request-channel' + sid);

channel.bind('merge-request-event', function(data) {
	$("#receivedMessage").text(data.message);
	$("#received").show();
	increaseRequestsNumber();
	setTimeout(function() {
		$('#received').hide();
	}, 4000);
});

function increaseRequestsNumber() {
	requestsNumber++;
	$("#requestsNumber").text(requestsNumber);
}


