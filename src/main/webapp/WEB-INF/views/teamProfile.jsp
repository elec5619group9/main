<%@ include file="/WEB-INF/views/navbar.jsp"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<c:url value="/resources/css/teamProfile.css"/>" rel="stylesheet">
<title>Team Profile</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-3"></div>
		<div class="col-lg-3">
			<c:forEach var="u" items="${users}" varStatus="loop3">
				<a href="/gfa/otherProfile/${u.sid}">
				<c:if test="${not empty u.profilePicture }"> 
				<img
					src="${u.profilePicture}"
					width="80" height="80" style="border-radius: 50%" id="show-avatar">
				</c:if>
				<c:if test="${empty u.profilePicture }">
				<img
					src="${pageContext.request.contextPath}/resources/images/default.png"
					width="80" height="80" style="border-radius: 50%" id="show-avatar">
				</c:if> 
				</a>
			</c:forEach>
		</div>
		<div class="col-lg-5">
			<h2>Team profile</h3>
			<div class="card">
				<div class="card-header">Team ${myTeam.id} Capacity:
					${myTeam.currentNumber}/${myTeam.capacity}</div>
				<ul class="list-group list-group-flush">
					<li class="list-group-item">
						<h5>Team description</h5> ${myTeam.description}
					</li>
					<li class="list-group-item">
						<h5>Expected skills</h5> <c:forEach var="expectedTag"
							items="${expectedTags}" varStatus="loop">
							<span class="badge badge-${expectedTag.tagColor }">${expectedTag.tagName}</span>
						</c:forEach>
					</li>
					<li class="list-group-item">
						<h5>Owned skills</h5> <c:forEach var="ownedTag"
							items="${ownedTags}" varStatus="loop">
							<span class="badge badge-${ownedTag.tagColor}">${ownedTag.tagName}</span>
						</c:forEach>
					</li>
				</ul>
			</div>
			<c:if test="${isLeader}">
				<a href="/gfa/updateTeamProfile" class="btn"
					role="button">Update Profile</a>
			</c:if>
		</div>
		<div class="col-lg-1"></div>
	</div>
</body>