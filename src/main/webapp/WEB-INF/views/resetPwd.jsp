<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
	<head>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link href="<c:url value="/resources/css/resetPwd.css"/>" rel="stylesheet">
    	<title>Reset Password</title>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-5">
					<div class="slogan">
						<!--  <img src="<c:url value="resources/images/bg_img.jpeg"/>"/>-->
						<h2>Reset Your Password!</h2>
					</div>					
				</div>
				<div class="col-md-7">
					<div class="resetPwdForm">
						<h1 class="resetPwdTitle">Reset Your Password</h1>
                		<div class="w-100"></div>
                		<p class="resetSid">Your Sid: <c:out value='<%=request.getSession().getAttribute("changePwdSid")%>'/> </p>
                		<FONT color="red"><form:errors path="SidError" /></FONT>
                		<form:form action="resetPwd" method="POST" commandName="resetPwdForm">
	                    	<div class="form-group" id="newPwd">
			                    <label for="password">New Password</label> <label for="markSign" style="color:red">*</label>                        	                   
			                    <p class="pwdTips">
			                       	The length should be at least 5 with at least one upper case letter, one lower case letter,
			                       	one special character, one digit and no whitespace.
			                    </p>
			                    <p class="errorTips"><FONT color="red"><form:errors path="resetPwd" /></FONT></p>
			                    <input type="password" class="form-control" id="resetPwd" name="resetPwd"/>
		                    </div>                        
	                        <div class="form-group" id="confirmNewPwd">
	                        	<label for="password">Confirm New Password</label> <label for="markSign" style="color:red">*</label>
		                        <p class="errorTips"><FONT color="red"><form:errors path="confirmPwd" /></FONT></p> 
		                        <input type="password" class="form-control" id="confirmPwd" name="confirmPwd"/>
		                        <button type="submit" id="restPwd_btn" class="btn">Submit</button>
	                        </div>
	                    </form:form>
	                </div>	                    
				</div>
			</div>			
		</div>
	</body>
</html>
