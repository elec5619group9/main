<%@ include file="/WEB-INF/views/navbar.jsp"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<c:url value="/resources/css/userProfile.css"/>" rel="stylesheet">
<title>My Profile</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-4">
		<c:if test="${not empty user.profilePicture }">
			<img
				src="${user.profilePicture}"
				 style="width: 60%; border-radius: 50%" />
		</c:if>
		<c:if test="${empty user.profilePicture }">
			<img
				src="${pageContext.request.contextPath}/resources/images/default.png"
				 style="width: 60%; border-radius: 50%" />
		</c:if>
		</div>
		<div class="col-lg-5">
			<h2>My profile</h2>
			<div class="card">
				<div class="card-header">
					My personal detail	
				</div>
				<ul class="list-group list-group-flush">
					<li class="list-group-item">
						<h5>sid</h5> ${user.sid}
					</li>
					<li class="list-group-item">
						<h5>Name</h5> ${user.firstName} ${user.lastName}
					</li>
					<li class="list-group-item">
						<h5>Contact Email</h5> ${user.emailAddress}
					</li>
					<li class="list-group-item">
						<h5>Self Introduction</h5> ${user.selfIntro}
					</li>
				</ul>
			</div>
			<a href="/gfa/updateProfile" class="btn" role="button">Update Profile</a>
		</div>
		<div class="col-lg-1"></div>
	</div>
</body>