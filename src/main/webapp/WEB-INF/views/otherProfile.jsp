<%@ include file="/WEB-INF/views/navbar.jsp"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<c:url value="/resources/css/otherProfile.css"/>" rel="stylesheet">
<title>Other Profile</title>
</head>
<body>
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-4">
		<c:if test="${not empty other.profilePicture }">
			<img
				src="${other.profilePicture}"
				alt="upload your photo" style="width: 60%; border-radius: 50%" />
		</c:if>
		<c:if test="${empty other.profilePicture}">
			<img
				src="${pageContext.request.contextPath}/resources/images/default.png"
				alt="upload your photo" style="width: 60%; border-radius: 50%" />
		</c:if>
		</div>
		<div class="col-lg-5">
			<h2>Classmate's Profile</h3>
			<div class="card">
				<div class="card-header"><h4>${other.firstName} personal detail</h4></div>
				<ul class="list-group list-group-flush">
					<li class="list-group-item">
						<h5>Sid</h5> ${other.sid}
					</li>
					<li class="list-group-item">
						<h5>Name</h5> ${other.firstName} ${other.lastName}
					</li>
					<li class="list-group-item">
						<h5>Contact Email</h5> ${other.emailAddress}
					</li>
					<li class="list-group-item">
						<h5>Self Introduction</h5> ${other.selfIntro}
					</li>
				</ul>
			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
</body>