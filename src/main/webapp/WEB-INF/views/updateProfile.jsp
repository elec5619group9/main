<%@ include file="/WEB-INF/views/navbar.jsp"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<c:url value="/resources/css/updateUserProfile.css"/>" rel="stylesheet">
<title>Update Profile</title>
</head>
<body>
	<form action="updateProfile" method="POST"
		enctype="multipart/form-data">
		<div class="row">
			<div class="col-lg-2"></div>
			<div class="col-lg-4">
				<c:if test="${not empty user.profilePicture }">
				<img
					src="${user.profilePicture}"
					 style="width: 60%; border-radius: 50%" id="show-avatar" />
				</c:if>
				<c:if test="${empty user.profilePicture }">
					<img
					src="${pageContext.request.contextPath}/resources/images/default.png"
					 style="width: 60%; border-radius: 50%" id="show-avatar" />
				</c:if>
					 <input
					type="file" class="form-control-file" id="avatar" name="avatar">
					<input type="hidden" name="profile_picture" id="profile_picture" value="">
			</div>
			<div class="col-lg-5">
				<div class="updateProfileForm">
					<h1 class="registerTitle">Update your profile</h1>
					<div class="form-group">
						<label for="firstname">First Name</label> <input type="text"
							class="form-control" id="firstname" name="firstname"
							placeholder="firstname" value="${ user.firstName}">
					</div>
					<div class="form-group">
						<label for="lastname">Last Name</label> <input type="text"
							class="form-control" id="lastname" name="lastname"
							placeholder="lastname" value="${user.lastName }">
					</div>
					<div class="form-group">
						<label for="intro">Self Introduction</label>
						<textarea rows="3" class="form-control" id="intro" name="intro"
							placeholder="Enter Self Intro">${user.selfIntro}</textarea>
					</div>
					<!-- <button type="submit" id="create_btn" class="btn">Update</button> -->
					<div class="outer">
						<div class="inner"><a class="btn" id="cancel_btn" role="button" href="<c:url value="/userProfile"/>">Cancel</a></div>                 
			 	        <div class="inner"><button type="submit" id="create_btn" class="btn">Update</button></div>
			 	    </div>
				</div>

			</div>
			<div class="col-lg-1"></div>

		</div>
	</form>
</body>
</html>
<script type="text/javascript"
	src="<c:url value='/resources/js/updateProfile.js'/>"></script>