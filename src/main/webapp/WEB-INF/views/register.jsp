<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
	<head>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link href="<c:url value="/resources/css/register.css"/>" rel="stylesheet">
    	<title>Register Account</title>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-5">
					
					<div class="slogan">
						<!--  <img src="<c:url value="resources/images/bg_img.jpeg"/>"/>-->
						<h2>Collaborate Happy, Learn Happy!</h2>
					</div>
				</div>
				<div class="col-md-7">
					<div class="SignUpForm">
						<h1 class="registerTitle">Team Initialization</h1>
						<form:form action="register" method="POST" commandName="registForm">
			                    <div class="form-group" id="RegisterSID">
			                        <label for="registerSid">Your Student Number</label> <label for="markSign" style="color:red">*</label>                      
			                        <p class="errorTips"><FONT color="red"><form:errors path="registerSid" /></FONT><p>                      
			                        <input type="text" class="form-control" id="registerSid" name="registerSid">
			                    </div>		                    
			                    <div class="form-group" id="RegisterEml">
			                        <label for="registerEmail">Email Address</label> <label for="markSign" style="color:red">*</label>
			                        <p class="errorTips"><FONT color="red"><form:errors path="registerEmail" /></FONT></p>
			                        <input type="email" class="form-control" id="registerEmail" name="registerEmail">
			                    </div>		                    
			                    <div class="form-group" id="RegisterPwd">
			                        <label for="registerPwd">Password</label> <label for="markSign" style="color:red">*</label>
			                        <p class="errorTips"><FONT color="red"><form:errors path="registerPwd" /></FONT></p>
			                        <p class="pwdTips">
			                        	The length should be at least 5 with at least one upper case letter, one lower case letter,
			                        	one special character, one digit and no whitespace.
			                        </p>
			                        <input type="password" class="form-control" id="registerPwd" name="registerPwd">
			                    </div>		                    
			                    <div class="form-group" id="CheckPwd">
			                        <label for="checkPwd">Confirm Password</label> <label for="markSign" style="color:red">*</label>
			                        <p class="errorTips"><FONT color="red"><form:errors path="checkPwd" /></FONT></p>
			                        <input type="password" class="form-control" id="checkPwd" name="checkPwd">
			                    </div>	
			                    <div class="form-group border-top" id="Questions_Title">
			                    	<p class="Set_security_Q">Please set your Security Questions for further security problems.</p>
			                    </div>                    
				                <div class="form-group" id="securityQ1">
				                    <label for="securityQ1">Question 1</label> <label for="markSign" style="color:red">*</label>
				                    <p class="securityQ">What is my uniKey?</p>
				                </div>
				                <div class="form-group" id="Q1Answer">
			                        <label for="Q1Answer1">Answer</label> <label for="markSign" style="color:red">*</label>
				                    <p class="errorTips"><FONT color="red"><form:errors path="registAnswer1" /></FONT></p>
				                    <input type="text" class="form-control" id="Q1Answer1" name="registAnswer1">
				                </div>		                    
				                <div class="form-group" id="securityQ2">
				                    <label for="securityQ1">Question 2</label> <label for="markSign" style="color:red">*</label>
				                    <p class="securityQ">What is your degree?</p>
			                    </div>                    
				                <div class="form-group" id="Q2Answer">
				                    <label for="Q2Answer2">Answer</label> <label for="markSign" style="color:red">*</label>
				                    <p class="errorTips"><FONT color="red"><form:errors path="registAnswer2" /></FONT></p>
				                    <input type="text" class="form-control" id="Q2Answer2" name="registAnswer2">
				                </div>
				                
				                <div class="outer">
				                	<div class="inner"><a class="btn" id="cancel_btn" role="button" href="<c:url value="/"/>">Cancel</a></div>                 
			 	                	<div class="inner"><button type="submit" id="signup_btn" class="btn">Sign up</button></div>
			 	                </div>
			            </form:form>
		            </div>
	            </div>
			</div>
		</div>
	</body>
</html>
