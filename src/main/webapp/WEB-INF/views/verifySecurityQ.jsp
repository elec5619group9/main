<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
	<head>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link href="<c:url value="/resources/css/verifySecurityQ.css"/>" rel="stylesheet">
    	<title>Verify Security Questions</title>
	</head>
	<body>
		<div class="container-fluid">
		<div class="row">
			<div class="col-md-5">
				
				<div class="slogan">
						<!--  <img src="<c:url value="resources/images/bg_img.jpeg"/>"/>-->
						<h2>Verify your account!</h2>
				</div>
				 
			</div>
			<div class="col-md-7">
				<div class="verifyQForm">
					<div class="verifyQTitle">
		                <h1>Verify Security Questions</h1>
		                <p>Please answer the following questions.</p>
		            </div>
					<form:form action="verifySecurityQ" method="POST" commandName="verifyQForm">
						<div class="form-group" id="VerifySid">
							<label for="verifySid">Sid</label> <label for="markSign" style="color:red">*</label>                      
		                    <p><FONT color="red"><form:errors path="verifySid" /></FONT></p>                      
		                    <input type="text" class="form-control" placeholder="Enter Sid" id="verifySid" name="verifySid">
		                </div>
		                
		                <div class="form-group" id="VerifyQ1">
		                    <label for="VerifyQ1">Question 1</label> <label for="markSign" style="color:red">*</label>
		                    <p class="securityQ">What is my uniKey?</p>
		                </div>
		                <div class="form-group" id="VerifyA1">
		                    <label for="AQ1Answer1">Answer</label> <label for="markSign" style="color:red">*</label>
		                    <FONT color="red"><form:errors path="verifyA1" /></FONT> 
		                    <input type="text" class="form-control" id="verifyA1" name="verifyA1">
		                </div>
		                    
		                <div class="form-group" id="VerifyQ2">
		                    <label for="VerifyQ2">Question 2</label> <label for="markSign" style="color:red">*</label>
		                    <p class="securityQ">What is my degree?</p>
		                </div>                    
		                <div class="form-group" id="VerifyA2">
		                    <label for="AQ2Answer2">Answer</label> <label for="markSign" style="color:red">*</label>
		                    <FONT color="red"><form:errors path="verifyA2" /></FONT> 
		                    <input type="text" class="form-control" id="verifyA2" name="verifyA2">
		                 </div>
		                 <div class="outer">
			                 <div class="inner"><a class="btn" id="cancel_btn" role="button" href="<c:url value="/"/>">Cancel</a></div>
			                 <div class="inner"><button type="submit" id="confirm_btn" class="btn">Confirm</button></div>
		                 </div>
		            </form:form>
	            </div>
            </div>
		</div>
		</div>
	</body>
</html>