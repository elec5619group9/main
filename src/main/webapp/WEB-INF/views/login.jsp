<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
	<head>
		<meta charset="utf-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link href="<c:url value="/resources/css/login.css"/>" rel="stylesheet">
    	<title>Log in</title>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-5">
					<div class="slogan">
						<!--  <img src="<c:url value="resources/images/bg_img.jpeg"/>"/>-->
						<h2>Collaborate Happy, Learn Happy!</h2>
					</div>					
				</div>
				<div class="col-md-7">
					<div class="SignInForm">
						<h1 class="signInTitle">Sign in</h1>
			            <!-- User input form to validate a user -->
						<form:form action="login" method="POST" commandName="loginForm">
			                <div class="form-group" id="LoginSid">
			                	<label for="loginSid">Student Id</label> <label for="markSign" style="color:red">*</label>                     
			                    <p class="errorTips"><FONT color="red"><form:errors path="loginSid" /></FONT></p>                      
			                    <input type="text" class="form-control" placeholder="Enter Sid" id="loginSid" name="loginSid">
			               </div>
			               <div class="form-group" id="LoginPassword">
			                    <label for="loginPwd">Password</label> <label for="markSign" style="color:red">*</label>                          
			                    <p class="errorTips"><FONT color="red"><form:errors path="loginPwd" /></FONT></p>                  
			                    <input type="password" class="form-control" placeholder="Enter password" id="loginPwd" name="loginPwd">
			               </div>
			               <p><a href="<c:url value="/verifySecurityQ"/>">Forgot your password?</a></p>
			               <p class="registLink">Don't have an account? <a href="register">Sign up</a></p>	
			               <button type="submit" id="login_btn" class="btn">Sign in</button>
			            </form:form>     
		            </div>      
				</div>
			</div>
		</div>
	</body>
</html>