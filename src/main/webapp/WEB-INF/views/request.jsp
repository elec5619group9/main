<%@ include file="/WEB-INF/views/navbar.jsp"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<c:url value="/resources/css/request.css"/>" rel="stylesheet">
<title>Handle Request</title>
</head>
	<body>
		<div class="container">
			<div id="overCap" class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none">
  				<strong>Danger!</strong> You cannot merge with this team, it is over capacity!
  				<button type="button" class="close" aria-label="Close" onclick="hideAlert()">
    			<span aria-hidden="true">&times;</span>
  				</button>
			</div>
			<div id="noTeam" class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none">
  				<strong>Danger!</strong> You cannot merge with this team, this team does not exist!
  				<button type="button" class="close" aria-label="Close" onclick="hideAlert()">
    			<span aria-hidden="true">&times;</span>
  				</button>
			</div>
			<div id="success" class="alert alert-success alert-dismissible fade show" role="alert" style="display: none">
  				<strong>Congratulation!</strong> You have some new members!
  				<div id="countDown"></div>
  				<button type="button" class="close" aria-label="Close" onclick="hideAlert()">
    			<span aria-hidden="true">&times;</span>
  				</button>
			</div>
			<h2>Received requests</h2>
				<table class="table table-hover">
					<thead class="thead-dark">
						<tr>
							<th scope="col">Request Id</th>
							<th scope="col">Sender Group Id</th>
							<th scope="col">Current Number</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="request" items="${requests}" varStatus="loop">
							<tr id="${request.id}">
								<td>${request.id}</td>
								<td>${request.senderId}</td>
								<td>${request.senderCurrentNumber}</td>
								<c:if test="${isLeader}">
									<td>
									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#teamModal${request.senderId}">Accept</button>
									<button type="button" class="btn btn-secondary" onclick="rejectTeam('${request.id}')">Reject</button>
									</td>
								</c:if>
								<c:if test="${not isLeader}">
									<td>
									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#teamModal${request.senderId}" disabled>Accept</button>
									<button type="button" class="btn btn-secondary" onclick="rejectTeam('${request.id}')" disabled>Reject</button>
									</td>
								</c:if>
								<!-- Modal -->
								<div class="modal fade" id="teamModal${request.senderId}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalLabel">Reminder</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">Are you sure you want to accept the request from team ${request.senderId}</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button id="merge" type="button" class="btn btn-primary" onclick="mergeTeam('${request.senderId}', '${request.receiverId}')">Accept</button>
											</div>
										</div>
									</div>
								</div>
							<tr>
						</c:forEach>
					</tbody>
			</table>
		</div>
	</body>
</html>
<script type="text/javascript" src="<c:url value='/resources/js/request.js'/>"></script>