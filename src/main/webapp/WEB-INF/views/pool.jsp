<%@ include file="/WEB-INF/views/navbar.jsp"%>
<html>
<head>
<link href="<c:url value="/resources/css/pool.css"/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<div id="received"
			class="alert alert-success alert-dismissible fade show" role="alert"
			style="display: none">
			<strong id="whoops">Whoops!</strong>
			<p id="receivedMessage"></p>
			<button type="button" class="close" aria-label="Close"
				onclick="hideAlert()">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div id="success"
			class="alert alert-success alert-dismissible fade show" role="alert"
			style="display: none">
			<strong>Congratulation!</strong> You have successfully sent the
			request!
			<button type="button" class="close" aria-label="Close"
				onclick="hideAlert()">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div id="warning"
			class="alert alert-warning alert-dismissible fade show" role="alert"
			style="display: none">
			<strong>Warning!</strong> You have already sent the request!
			<button type="button" class="close" aria-label="Close"
				onclick="hideAlert()">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>

		<div class="container">
			<div class="row" id="infoRow">
				<div class="col-sm">
					<h3>My team</h3>
					<div class="card">
						<div class="card-header">Team ${myTeam.id} Capacity:
							${myTeam.currentNumber}/${myTeam.capacity}</div>
						<ul class="list-group list-group-flush">
							<li class="list-group-item">
								<h5>Team description</h5> ${myTeam.description}
							</li>
							<li class="list-group-item">
								<h5>Expected skills</h5> <c:forEach var="expectedTag"
									items="${expectedTags}" varStatus="loop">
									<span class="badge badge-${expectedTag.tagColor }"
										id="specificSkills">${expectedTag.tagName}</span>
								</c:forEach>
							</li>
							<li class="list-group-item">
								<h5>Owned skills</h5> <c:forEach var="ownedTag"
									items="${ownedTags}" varStatus="loop">
									<span class="badge badge-${ownedTag.tagColor}"
										id="specificSkills">${ownedTag.tagName}</span>
								</c:forEach>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-sm" id="filterForOther">
					<div class="otherFilter">
						<h3 class="other-teams-title">Other teams</h3>
						<div class="search-box">
							<input id="search" type="text" class="form-control my-search-tag"
								placeholder="Search something">
						</div>
					</div>

				</div>

				<div class="col-sm">
					<h3 class="filter-expected-skills">Filter for expected skills</h3>
					<div class="form-group badge-checkboxes" id="expectedSkills">
						<c:forEach var="tag" items="${allTags}" varStatus="eTagLoop">
							<div
								class="form-check form-check-inline badge badge-${tag.tagColor} badge-light">
								<input class="form-check-input expected-checks" type="checkbox"
									id="inlineCheckbox${eTagLoop.index}" value="${tag.tagName}"
									style="display: none"> <label class="form-check-label"
									for="inlineCheckbox${eTagLoop.index}">${tag.tagName}</label>
							</div>
						</c:forEach>
					</div>

					<h3 class="filter-owned-skills" id="ownedSkills">Filter for
						owned skills</h3>
					<div class="form-group badge-checkboxes">
						<c:forEach var="tag" items="${allTags}" varStatus="oTagLoop">
							<div
								class="form-check form-check-inline badge badge-${tag.tagColor} badge-light">
								<input class="form-check-input owned-checks" type="checkbox"
									id="lineCheckbox${oTagLoop.index}" value="${tag.tagName}"
									style="display: none"> <label class="form-check-label"
									for="lineCheckbox${oTagLoop.index}">${tag.tagName}</label>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>
		</div>


		<div class="other-teams-item">
			<c:forEach var="team" items="${teams}" varStatus="loop">
				<div class="card other-teams-info" id="team${loop.index}">
					<div class="card-header">
						Team ${team.id} Capacity: ${team.currentNumber}/${team.capacity}
						<c:if test="${isLeader}">
							<td>
								<button type="button" class="btn" id="mergeRequestBtn"
									data-toggle="modal" data-target="#teamModal${team.id}">Merge
								</button>
							</td>
						</c:if>
						<c:if test="${not isLeader}">
							<td>
								<button type="button" class="btn btn-primary"
									data-toggle="modal" data-target="#teamModal${team.id}" disabled>Merge
								</button>
							</td>
						</c:if>
					</div>
					<ul class="list-group list-group-flush list-group-horizontal">
						<li class="list-group-item" id="teamDescription">
							<h5>Team description</h5> ${team.description}
						</li>
						<li class="list-group-item" id="teamMember"><c:forEach
								var="user" items="${listOfUsers[loop.index]}" varStatus="loop3">
								<a href="/gfa/otherProfile/${user.sid}"> <c:if
										test="${not empty user.profilePicture }">
										<img src="${user.profilePicture}" width="80" height="80"
											style="border-radius: 50%" id="show-avatar">
									</c:if> <c:if test="${empty user.profilePicture }">
										<img
											src="${pageContext.request.contextPath}/resources/images/default.png"
											width="80" height="80" style="border-radius: 50%"
											id="show-avatar">
									</c:if>
								</a>
							</c:forEach></li>
						<li class="list-group-item tagsFix"
							id="expected-tags${loop.index}">
							<h5 id="skillsTile">Expected skills</h5> <c:forEach
								var="expectedTag" items="${listOfExpectedTags[loop.index]}"
								varStatus="loop1">
								<span class="badge badge-${expectedTag.tagColor} "
									id="specificSkills">${expectedTag.tagName}</span>
							</c:forEach>
						</li>
						<li class="list-group-item tagsFix" id="owned-tags${loop.index}">
							<h5 id="skillsTile">Owned skills</h5> <c:forEach var="ownedTag"
								items="${listOfOwnedTags[loop.index]}" varStatus="loop2">
								<span class="badge badge-${ownedTag.tagColor} "
									id="specificSkills">${ownedTag.tagName}</span>
							</c:forEach>
						</li>
					</ul>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="teamModal${team.id}" tabindex="-1"
					aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Reminder</h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">Are you sure you want to merge to
								team ${team.id}</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary"
									data-dismiss="modal">Close</button>
								<button id="merge" type="button" class="btn btn-primary"
									onclick="sendRequest('${myTeam.id}', '${team.id}', '${myTeam.currentNumber}')">Merge
								</button>
							</div>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>

	</div>
</body>
</html>
<script>
	var sid = "${sid}";
	var requestsNumber = "${numOfRequests}";
	var noOfTeams = "${noOfTeams}";
</script>
<script type="text/javascript"
	src="<c:url value='/resources/js/pool.js'/>"></script>