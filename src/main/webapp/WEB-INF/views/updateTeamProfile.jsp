<%@ include file="/WEB-INF/views/navbar.jsp"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<c:url value="/resources/css/updateTeamProfile.css"/>" rel="stylesheet">
<title>Update Team Profile</title>
</head>
<body>

	<form action="updateTeamProfile" method="POST"
		enctype="multipart/form-data">
		<div class="row">
			<div class="col-lg-2"></div>
			<div class="col-lg-4">
				<h1>Change leader</h1>
				<div class="row">
					<div class="cap">
						<c:forEach var="u" items="${users}" varStatus="loop3">
							<c:if test="${u.isLeader}">
							<i class="fa fa-graduation-cap" aria-hidden="true" id="cap${u.sid}"></i>
							</c:if>
							<c:if test="${not u.isLeader}">
							<i class="fa fa-graduation-cap" aria-hidden="true" id="cap${u.sid}" style="display:none"></i>
							</c:if>
							<label class="leader" style="cursor:pointer;">
							<c:if test="${not empty u.profilePicture }">
							<img
								src="${u.profilePicture}"
								width="80" height="80" style="border-radius: 50%"
								id="show-avatar">
							</c:if>
							<c:if test="${empty u.profilePicture }">
							<img src="${pageContext.request.contextPath}/resources/images/default.png" width="80" height="80" style="border-radius: 50%"
								id="show-avatar">
							</c:if>
							<c:if test="${u.isLeader}">
							<input class="leader-radio" type="radio" name="leader" value="${u.sid}" style="display:none" checked>
							</c:if>
							<c:if test="${not u.isLeader}">
							<input class="leader-radio" type="radio" name="leader" value="${u.sid}" style="display:none">
							</c:if>
							</label>
						</c:forEach>
					</div>
				</div>
			</div>
			<div class="col-lg-5">
				<div class="updateProfileForm">
					<h1 class="registerTitle">Update your profile</h1>

					<div class="form-group" id="teamCapacity">
						<h4 for="teamcap">Choose Team Capacity</h4> <br>
						<button style="display: inline-block;" id="min" type="button"
							onclick="mincap()">-</button>
						<input type="text" id="teamcap" name="capacity"
							value="${team.capacity}" readonly>
						<button style="display: inline-block;" id="plus" type="button"
							onclick="pluscap()">+</button>
					</div>

					<div class="form-group" id="teamDescription">
						<h4 for="description">Enter team description</h4><br>
						<textarea class="form-control" id="description" name="description"
							rows="3">${team.description }</textarea>
					</div>
					<!-- <button type="submit" id="update_btn" class="btn btn-primary">Update</button> -->
					<div class="outer">
						<div class="inner"><a class="btn" id="cancel_btn" role="button" href="<c:url value="/teamProfile"/>">Cancel</a></div>                 
			 	        <div class="inner"><button type="submit" id="update_btn" class="btn">Update</button></div>
			 	    </div>
				</div>

			</div>
			<div class="col-lg-1"></div>

		</div>
	</form>
</body>
</html>
<script type="text/javascript"
	src="<c:url value='/resources/js/updateTeamProfile.js'/>"></script>