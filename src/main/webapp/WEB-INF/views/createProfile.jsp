<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<c:url value="/resources/css/createProfile.css"/>"
	rel="stylesheet">
<title>Create Profile</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-5">

				<div class="slogan">
					<!--  <img src="<c:url value="resources/images/bg_img.jpeg"/>"/>-->
					<h2>Collaborate Happy, Learn Happy!</h2>
				</div>
			</div>
			<div class="col-md-7">
				<div class="createProfileForm">
					<h1 class="registerTitle">Create your profile</h1>
					<form action="createProfile" method="POST"
						enctype="multipart/form-data">
						<div class="form-group">
							<label for="avatar">Your avatar</label>
							<div class="form-group" id="chooseFile">
								<label class="custom-file-upload"> <input type="file"
									class="form-control-file" id="avatar" name="avatar"
									>
								</label>
							</div>
							<div class="photo">
								<img src="resources/images/default.png" width="170" height="170"
									style="border-radius: 50%" id="show-avatar"> 
									<input
									type="hidden" name="profile_picture" id="profile_picture"
									value="">
							</div>
						</div>
						<div class="form-group">
							<label for="firstname">First Name</label> <input type="text"
								class="form-control" id="firstname" name="firstname"
								placeholder="firstname">
						</div>
						<div class="form-group">
							<label for="lastname">Last Name</label> <input type="text"
								class="form-control" id="lastname" name="lastname"
								placeholder="lastname">
						</div>
						<div class="form-group">
							<label for="intro">Self Introduction</label>
							<textarea rows="3" class="form-control" id="intro" name="intro"
								placeholder="Enter Self Intro"></textarea>
						</div>
						<button type="submit" id="create_btn" class="btn">Create</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript"
	src="<c:url value='/resources/js/createProfile.js'/>"></script>