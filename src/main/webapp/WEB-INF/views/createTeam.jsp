<%@ include file="/WEB-INF/views/include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<c:url value="/resources/css/createTeam.css"/>"
	rel="stylesheet">
<title>Team Initialization</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-5">

				<div class="slogan">
					<!--  <img src="<c:url value="resources/images/bg_img.jpeg"/>"/>-->
					<h2>Start your team!</h2>
				</div>
			</div>
			<div class="col-md-7">
				<div class="teamInitializationForm">
					<h1 class="registerTitle">Team Initialization</h1>
					<form action="createTeam" method="POST">
						<div class="form-group" id="selectCourse">
							<label for="selectCourse">Select Course</label> <select
								class="form-control" id="selectCourse" name="course">
								<option value="1">ELEC5619</option>
							</select>
						</div>
						<div class="form-group" id="chooseCapacity">
							<label for="teamcap">Choose Team Capacity</label> <br>
							<button style="display: inline-block;" id="min" type="button"
								onclick="mincap()">-</button>
							<input type="text" id="teamcap" name="capacity" value="1"
								readonly>
							<button style="display: inline-block;" id="plus" type="button"
								onclick="pluscap()">+</button>
						</div>
						<div class="form-group" id="expectedSkills">
							<label for="expectedTag">Choose Expected Skills</label>
							<div class="form-group badge-checkboxes" id="expectedTag">
								<c:forEach var="tag" items="${allTags}" varStatus="eTagLoop">
									<div
										class="form-check form-check-inline badge badge-${tag.tagColor} badge-light">
										<input class="form-check-input expected-checks"
											type="checkbox" id="inlineCheckbox${eTagLoop.index}"
											 style="display: none" name="expectedTag${eTagLoop.index}"> <label
											class="form-check-label"
											for="inlineCheckbox${eTagLoop.index}">${tag.tagName}</label>
									</div>
								</c:forEach>
							</div>
						</div>
						<div class="form-group" id="ownedSkills">
							<label for="ownedTag">Choose Owned Skills</label>
							<div class="form-group badge-checkboxes" id="ownedTag">
								<c:forEach var="tag" items="${allTags}" varStatus="oTagLoop">
									<div
										class="form-check form-check-inline badge badge-${tag.tagColor} badge-light">
										<input class="form-check-input owned-checks" type="checkbox"
											id="lineCheckbox${oTagLoop.index}" 
											style="display: none" name="ownedTag${oTagLoop.index }"> <label
											class="form-check-label" for="lineCheckbox${oTagLoop.index}">${tag.tagName}</label>
									</div>
								</c:forEach>
							</div>
						</div>
						<div class="form-group">
							<label for="description">Enter team description</label>
							<textarea class="form-control" id="description" name="description" placeholder="Please introduce your group!" rows="3"></textarea>
						</div>
						<button type="submit" id="init_btn" class="btn">Initialize</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript"
	src="<c:url value='/resources/js/createTeam.js'/>"></script>