<%@ page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS only -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="<c:url value="/resources/css/navbar.css"/>" rel="stylesheet">
<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
</head> 
<body>

<nav class="navbar navbar-expand-xl navbar-dark bg-dark">
	<a href="/gfa/pool" class="navbar-brand"><i class="fa fa-users"></i>Group<b>Formation</b>Assistant</a>  		
	<button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
		<span class="navbar-toggler-icon"></span>
	</button>
	<!-- Collection of nav links, forms, and other content for toggling -->
	<div id="navbarCollapse" class="collapse navbar-collapse justify-content-start">		
		<div class="navbar-nav ml-auto">
			<a href="/gfa/pool" class="nav-item nav-link"><i class="fa fa-home"></i><span>Home</span></a>		
			<a href="/gfa/request" class="nav-item nav-link"><i class="fa fa-bell"></i><span>Requests</span><span class="badge badge-danger" id="requestsNumber">${numOfRequests}</span></a>
			<div class="nav-item dropdown">
				<a href="#" data-toggle="dropdown" class="nav-item nav-link dropdown-toggle user-action">
				<c:if test="${not empty user.profilePicture }">
				<img src="${user.profilePicture}" class="avatar" alt="Avatar">
				</c:if>
				<c:if test="${empty user.profilePicture }">
				<img src="${pageContext.request.contextPath}/resources/images/default.png" class="avatar" alt="Avatar">
				</c:if>
				 ${firstName}  <b class="caret"></b></a>
				<div class="dropdown-menu">
					<a href="/gfa/userProfile" class="dropdown-item"><i class="fa fa-user-o"></i> My Profile</a>
					<div class="divider dropdown-divider"></div>
					<a href="/gfa/teamProfile" class="dropdown-item"><i class="fa fa-users"></i> Team Profile</a>
					<div class="divider dropdown-divider"></div>
					<a href="/gfa/logout" class="dropdown-item"><i class="material-icons">&#xE8AC;</i> Logout</a>
				</div>
			</div>
		</div>
	</div>
</nav>
</body>
</html>