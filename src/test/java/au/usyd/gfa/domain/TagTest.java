package au.usyd.gfa.domain;

import junit.framework.TestCase;

public class TagTest extends TestCase {
    private Tag tag;

    protected void setUp() throws Exception {
        tag = new Tag();
    }

    public void TestGetandSetTagName() {
        long testTagId = 1;
        String testTagName= "Java";
        assertNull(tag.getTagName());
        // check if default getId value equals to 0
        tag.setTagName(testTagName);
        assertEquals(testTagName, tag.getTagName());
        // check if the first tag name equals to previous set 'Java'
    }


    public void TestGetandSetTagColor() {
        String testTagColor= "Blue";
        assertNull(tag.getTagColor());
        // check if default getId value equals to 0
        tag.setTagColor(testTagColor);
        assertEquals(testTagColor, tag.getTagColor());
        // check if the tag color equals to previous set 'Blue'
    }
    public void TestGetandSetCourseId() {
        long testTagCourseId= 1;
        assertEquals(0,tag.getCourseId());
        // check if default getId value equals to 0
        tag.setCourseId(testTagCourseId);
        assertEquals(testTagCourseId, tag.getCourseId());
        // check if the tag CourseId equals to previous set '1'
    }

}