package au.usyd.gfa.domain;

import junit.framework.TestCase;

public class RequestTest extends TestCase {
	private Request request;
	
	protected void setUp() throws Exception {
		request = new Request();
	}
	
	public void testSetAndGetSenderId() {
		long testSenderId = 1;
		assertEquals(0, request.getSenderId());
		request.setSenderId(testSenderId);
		assertEquals(testSenderId, request.getSenderId());
	}
	
	public void testSetAndGetReceiverId() {
		long testReceiverId = 2;
		assertEquals(0, request.getReceiverId());
		request.setReceiverId(testReceiverId);
		assertEquals(testReceiverId, request.getReceiverId());
	}
	
	public void testSetAndGetSenderCurrentNumber() {
		long testSenderCurrentNumber = 4;
		assertEquals(0, request.getSenderCurrentNumber());
		request.setSenderCurrentNumber(testSenderCurrentNumber);
		assertEquals(testSenderCurrentNumber, request.getSenderCurrentNumber());
	}
	

}
