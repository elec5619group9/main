package au.usyd.gfa.domain;

import junit.framework.TestCase;

/**
 * unit test for loginForm
 * testing the Getter and Setter methods
 * 
 * @author ulricawu
 * */
public class LoginFormTest extends TestCase{
	private LoginForm loginForm;
	
	protected void setUp() throws Exception{
		loginForm = new LoginForm();
	}
	
	public void testSetAndGetLoginSid() {
		String loginSid = "1234";
		assertNull(loginForm.getLoginSid());
		loginForm.setLoginSid(loginSid);
		assertEquals(loginSid, loginForm.getLoginSid());
	}
	
	public void testSetAndGetLoginPwd() {
		String loginPwd = "99PP--33";
		assertNull(loginForm.getLoginPwd());
		loginForm.setLoginPwd(loginPwd);
		assertEquals(loginPwd, loginForm.getLoginPwd());
	}
}
