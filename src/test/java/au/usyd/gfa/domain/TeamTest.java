package au.usyd.gfa.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class TeamTest {
	private Team team;
	
	@Test
	public void testSetAndGetId() {
		team = new Team();
		long testId = 1;
		team.setId(testId);
		assertEquals(testId, team.getId());
	}
	
	@Test
	public void testSetAndGetCapacity() {
		team = new Team();
		int testCapacity = 1;
		team.setCapacity(testCapacity);
		assertEquals(testCapacity, team.getCapacity());
	}
	@Test
	public void testSetAndGetCurrentNumber() {
		team = new Team();
		int CurrentNumber = 1;
		team.setCurrentNumber(CurrentNumber);
		assertEquals(CurrentNumber, team.getCurrentNumber());
	}
	@Test
	public void testSetAndGetCourseId() {
		team = new Team();
		long CourseId = 1;
		team.setCourseId(CourseId);
		assertEquals(CourseId, team.getCourseId());
	}
	
}
