package au.usyd.gfa.domain;

import junit.framework.TestCase;

/**
 * unit test for registForm
 * testing the Getter and Setter methods
 * 
 * @author ulricawu
 * */
public class RegistFormTest extends TestCase{
	private RegistForm registForm;
	
	protected void setUp() throws Exception{
		registForm = new RegistForm();
	}
	
	public void testSetAndGetRegisterSid() {
		String registSid = "1234";
		assertNull(registForm.getRegisterSid());
		registForm.setRegisterSid(registSid);
		assertEquals(registSid, registForm.getRegisterSid());
	}
	
	public void testSetAndGetRegisterEmail() {
		String registEmail = "hhh@gmail.com";
		assertNull(registForm.getRegisterEmail());
		registForm.setRegisterEmail(registEmail);
		assertEquals(registEmail, registForm.getRegisterEmail());
	}
	
	public void testSetAndGetRegisterPwd() {
		String registPwd = "hhh--II123";
		assertNull(registForm.getRegisterPwd());
		registForm.setRegisterPwd(registPwd);;
		assertEquals(registPwd, registForm.getRegisterPwd());
	}
	
	public void testSetAndGetCheckPwd() {
		String checkPwd = "hhh--II123";
		assertNull(registForm.getCheckPwd());
		registForm.setCheckPwd(checkPwd);;
		assertEquals(checkPwd, registForm.getCheckPwd());
	}
	
	public void testSetAndGetRegistAnswer1() {
		String registAnswer1 = "asdsa";
		assertNull(registForm.getRegistAnswer1());
		registForm.setRegistAnswer1(registAnswer1);;;
		assertEquals(registAnswer1, registForm.getRegistAnswer1());
	}
	
	public void testSetAndGetRegistAnswer2() {
		String registAnswer2 = "ASDASD";
		assertNull(registForm.getRegistAnswer2());
		registForm.setRegistAnswer2(registAnswer2);;;
		assertEquals(registAnswer2, registForm.getRegistAnswer2());
	}
	
}
