package au.usyd.gfa.domain;

import junit.framework.TestCase;

/**
 * unit test for resetPwdForm
 * testing the Getter and Setter methods
 * 
 * @author ulricawu
 * */
public class ResetPwdFormTest extends TestCase{
	private ResetPwdForm resetPwdForm;
	
	protected void setUp() throws Exception{
		resetPwdForm = new ResetPwdForm();
	}
	
	public void testSetAndGetResetPwd() {
		String resetPwd = "gg66GG--12";
		assertNull(resetPwdForm.getResetPwd());
		resetPwdForm.setResetPwd(resetPwd);;
		assertEquals(resetPwd, resetPwdForm.getResetPwd());
	}

	public void testSetAndGetConfirmPwd() {
		String confirmPwd = "gg66GG--12";
		assertNull(resetPwdForm.getConfirmPwd());
		resetPwdForm.setConfirmPwd(confirmPwd);;
		assertEquals(confirmPwd, resetPwdForm.getConfirmPwd());
	}
}
