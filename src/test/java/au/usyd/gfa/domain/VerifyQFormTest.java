package au.usyd.gfa.domain;

import junit.framework.TestCase;

/**
 * unit test for verifyQForm
 * testing the Getter and Setter methods
 * 
 * @author ulricawu
 * */
public class VerifyQFormTest extends TestCase{
	private VerifyQForm verifyQForm;
	
	protected void setUp() throws Exception{
		verifyQForm = new VerifyQForm();
	}
	
	public void testSetAndGetVerifySid() {
		String verifySid = "1234";
		assertNull(verifyQForm.getVerifySid());
		verifyQForm.setVerifySid(verifySid);
		assertEquals(verifySid, verifyQForm.getVerifySid());
	}
	
	public void testSetAndGetVerifyA1() {
		String verifyA1 = "12adasd";
		assertNull(verifyQForm.getVerifyA1());
		verifyQForm.setVerifyA1(verifyA1);;
		assertEquals(verifyA1, verifyQForm.getVerifyA1());
	}
	
	public void testSetAndGetVerifyA2() {
		String verifyA2 = "34adasd";
		assertNull(verifyQForm.getVerifyA2());
		verifyQForm.setVerifyA2(verifyA2);;
		assertEquals(verifyA2, verifyQForm.getVerifyA2());
	}
	

}
