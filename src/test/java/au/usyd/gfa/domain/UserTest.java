package au.usyd.gfa.domain;

import junit.framework.TestCase;

/**
 * unit test for user domain
 * testing the Getter and Setter methods
 * 
 * @author ulricawu
 * */
public class UserTest extends TestCase{
	
	private User user;
	
	protected void setUp() throws Exception{
		user = new User();
	}
	
	public void testGetSId() {
		assertNotNull(user.getSid());
	}
	
	public void testSetAndGetEmailAddress() {
		String emailAddress = "hhhhh@gggg.com";
		assertNull(user.getEmailAddress());
		user.setEmailAddress(emailAddress);
		assertEquals(emailAddress, user.getEmailAddress());
	}
	
	public void testSetAndGetFirstName() {
		String firstname = "John";
		assertNull(user.getfirstName());
		user.setFirstName(firstname);
		assertEquals(firstname, user.getfirstName());
	}
	
	public void testSetAndGetLastName() {
		String lastname = "Black";
		assertNull(user.getlastName());
		user.setLastName(lastname);
		assertEquals(lastname, user.getlastName());
	}
	
	public void testSetAndGetPassword() {
		String pwd = "Poo00--22";
		assertNull(user.getPassword());
		user.setPassword(pwd);
		assertEquals(pwd, user.getPassword());
	}
	
	public void testSetAndGetSecurityAnswer1() {
		String answer1 = "777888";
		assertNull(user.getSecurityAnswer1());
		user.setSecurityAnswer1(answer1);
		assertEquals(answer1, user.getSecurityAnswer1());
	}
	
	public void testSetAndGetSecurityAnswer2() {
		String answer2 = "kkkkkk";
		assertNull(user.getSecurityAnswer2());
		user.setSecurityAnswer2(answer2);
		assertEquals(answer2, user.getSecurityAnswer2());
	}
	
	public void testGetTeamId() {
		assertNotNull(user.getTeamId());
	}
	
	public void testSetAndGetIsLeader() {
		assertNotNull(user.getIsLeader());
	}
}