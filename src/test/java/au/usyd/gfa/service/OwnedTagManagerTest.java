package au.usyd.gfa.service;


import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import au.usyd.gfa.domain.OwnedTag;
import au.usyd.gfa.domain.Request;

public class OwnedTagManagerTest extends TestCase {
	
	private SimpleOwnedTagManager simpleownedTagManager;
	
	private List<OwnedTag> ownedTags;
	
	protected void setUp() throws Exception {
		simpleownedTagManager = new SimpleOwnedTagManager();
		ownedTags = new ArrayList<OwnedTag>();
		
		for(int i=0; i<3;i++) {
			OwnedTag tag = new OwnedTag();
			tag.setTeamId(1);
			tag.setTagId(i);
			ownedTags.add(tag);
			System.out.println(ownedTags.get(i).getTagId());
		}
		simpleownedTagManager.setTags(ownedTags);
		}
	
		public void testGetownedTags() {
			List<OwnedTag> testtags = simpleownedTagManager.getOwnedTags((long) 1);
			for (int i = 0; i < 3; i++) {
				assertEquals(i, testtags.get(i).getTagId());
			}

		}
		
		public void testAddAndDeleteownedTags() {
			OwnedTag ownedTag = new OwnedTag();
			ownedTag.setTeamId(2);
			ownedTag.setTagId(4);
			simpleownedTagManager.addOwnedTag(ownedTag);
			assertEquals(1, simpleownedTagManager.getOwnedTags((long) 2).size());
			simpleownedTagManager.deleteOwnedTag(ownedTag);
			assertEquals(0, simpleownedTagManager.getOwnedTags((long) 2).size());
			assertEquals(3, simpleownedTagManager.getOwnedTags((long) 1).size());
		}
		
		public void testUpdateownedTags() {
			OwnedTag ownedTag = new OwnedTag();
			ownedTag.setTeamId(3);
			ownedTag.setTagId(5);
			simpleownedTagManager.updateOwnedTag(ownedTag);
			List<OwnedTag> testtags = simpleownedTagManager.getOwnedTags((long)3);
			assertEquals(1, testtags.size());
			assertEquals(5, testtags.get(0).getTagId());
			
			simpleownedTagManager.updateOwnedTag(simpleownedTagManager.getOwnedTags((long)3).get(0));
			assertEquals(1, testtags.size());
			assertEquals(5, testtags.get(0).getTagId());
			

		}
			
}
