package au.usyd.gfa.service;

import au.usyd.gfa.dao.UserDAO;
import au.usyd.gfa.domain.RegistForm;
import au.usyd.gfa.domain.User;
import junit.framework.TestCase;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

/**
 * unit tests for authentication Service
 * testing different methods used in the service layer, like Retrieving User, Updating User, Adding user etc
 * 
 * @author ulricawu
 * */
@RunWith(MockitoJUnitRunner.class)
public class AuthenticationTest extends TestCase{
	
	@InjectMocks
	private AuthenServiceImple authenService;
	
	@Mock
	private UserDAO userDao;
	
	@Mock
	private Errors errors;
	
	@Before
	public void setUp() throws Exception{
		authenService = new AuthenServiceImple();
		authenService.setLoginDAO(userDao);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testGetUser() {
		assertNull(authenService.getUser(547));
		
		User user = new User();
		user.setSid(547);
		user.setEmailAddress("elec5619@gmail.com");
		user.setPassword("PPww**111");
		user.setFirstName("Jerry");
		user.setLastName("White");
		user.setSecurityAnswer1("Check1");
		user.setSecurityAnswer2("Check2");
		user.setTeamId(888);
		user.setIsLeader(false);
		
		Mockito.when(userDao.getUser(547)).thenReturn(user);
		User retrievedUser = authenService.getUser(547);
		assertEquals(user, retrievedUser);
	}
	
	@Test
	public void testAddUser() {
		User user = new User();
		user.setSid(666);
		user.setEmailAddress("Group9@gmail.com");
		user.setPassword("TTyy$$22");
		user.setFirstName("Kitty");
		user.setLastName("White");
		user.setSecurityAnswer1("Test3");
		user.setSecurityAnswer2("Test4");
		user.setTeamId(777);
		user.setIsLeader(false);
		
		Mockito.doNothing().when(userDao).addUser(user, errors);;
		authenService.addUser(user, errors);		
		verify(userDao, times(1)).addUser(user, errors);		
	}
	
	@Test
	public void testUpdateUser() {
		User user = new User();
		user.setSid(123);
		user.setEmailAddress("hh@gmail.com");
		user.setPassword("hhh--QQ123");
		user.setFirstName("Tom");
		user.setLastName("Black");
		user.setSecurityAnswer1("test1");
		user.setSecurityAnswer2("test2");
		user.setTeamId(999);
		user.setIsLeader(true);		
		Mockito.when(userDao.getUser(123)).thenReturn(user);
		
		String newPwd = "mmm-WWW999";
		
		Mockito.doNothing().when(userDao).updateUser("123", newPwd, errors);
		authenService.updateUser("123", newPwd, errors);
		verify(userDao, times(1)).updateUser("123", newPwd, errors);
	}
	
	@Test
	public void testCheckVerifyAnswer() {
		User user = new User();	
		user.setSid(123);
		user.setEmailAddress("hh@gmail.com");
		user.setPassword("hhh--QQ123");
		user.setFirstName("Tom");
		user.setLastName("Black");
		user.setSecurityAnswer1("test1");
		user.setSecurityAnswer2("test2");
		user.setTeamId(999);
		user.setIsLeader(true);
		Mockito.when(userDao.getUser(123)).thenReturn(user);
		
		String testAnswer1 = "test1";
		String testAnswer2 = "test2";
		
		Mockito.doNothing().when(userDao).checkVerifyAnswer("123", testAnswer1, testAnswer2, errors);
		authenService.checkVerifyAnswer("123", testAnswer1, testAnswer2, errors);
		
		verify(userDao, times(1)).checkVerifyAnswer("123", testAnswer1, testAnswer2, errors);
	}
	
	@Test
	public void testCheckLoginInfo() {
		User user = new User();	
		user.setSid(888);
		user.setEmailAddress("hh@gmail.com");
		user.setPassword("hhh--QQ123");
		user.setFirstName("Tom");
		user.setLastName("Black");
		user.setSecurityAnswer1("test1");
		user.setSecurityAnswer2("test2");
		user.setTeamId(123);
		user.setIsLeader(true);
		Mockito.when(userDao.getUser(123)).thenReturn(user);
		
		String testPwd = user.getPassword();
		
		Mockito.doNothing().when(userDao).checkLoginInfo("888", user.getPassword(), errors);
		authenService.checkLoginInfo("888", user.getPassword(), errors);		
		verify(userDao, times(1)).checkLoginInfo("888", testPwd, errors);
	}
}
