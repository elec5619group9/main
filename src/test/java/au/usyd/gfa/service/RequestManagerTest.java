package au.usyd.gfa.service;


import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import au.usyd.gfa.domain.Request;

public class RequestManagerTest extends TestCase {
	private SimpleRequestManager requestManager;
	
	private List<Request> requests;
	
	protected void setUp() throws Exception {
		requestManager = new SimpleRequestManager();
		requests = new ArrayList<Request>();
		
		Request request = new Request();
		request.setSenderId(1);
		request.setReceiverId(2);
		request.setSenderCurrentNumber(4);
		requests.add(request);
		
		request = new Request();
		request.setSenderId(3);
		request.setReceiverId(2);
		request.setSenderCurrentNumber(5);
		requests.add(request);
		
		request = new Request();
		request.setSenderId(1);
		request.setReceiverId(3);
		request.setSenderCurrentNumber(4);
		requests.add(request);
		
		requestManager.setRequests(requests);
	}
	
	
	public void testGetRequest() {
		List<Request> result2 = requestManager.getRequests((long)2);
		assertEquals(1, result2.get(0).getSenderId());
		assertEquals(3, result2.get(1).getSenderId());
	}
	
	public void testAddRequest() {
		
		assertEquals(false, requestManager.addRequest((long)1, (long)2, (long)4));
		assertEquals(true, requestManager.addRequest((long)2, (long)3, (long)4));
		System.out.println(requests.size());
	}
	
	public void testGetRequestNumber() {
		assertEquals(2, requestManager.getRequestNumber((long)2));
		assertEquals(0, requestManager.getRequestNumber((long)1));
		assertEquals(1, requestManager.getRequestNumber((long)3));
	}

}
