package au.usyd.gfa.service;

import au.usyd.gfa.dao.UserDAO;
import au.usyd.gfa.dao.UserDAOImpl;
import au.usyd.gfa.domain.Request;
import au.usyd.gfa.domain.Team;
import au.usyd.gfa.domain.User;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.management.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Errors;


@RunWith(MockitoJUnitRunner.class)
public class UserManagerTest extends TestCase {
		
	@InjectMocks
	private UserManager userManager;
	private SimpleUserManager simpleUserManager;
	
	@Mock
	private UserDAO userDao;
	
	@Mock
	private Errors errors;
	

	@Before
	public void setUp() throws Exception{
		userManager = new UserManager();
		userManager.setLoginDAO(userDao);	
		simpleUserManager = new SimpleUserManager();
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testGetUser() {
		assertNull(userManager.getUser(123));
		User user=new User();
		user.setSid(123);
		Mockito.when(userManager.getUser(123)).thenReturn(user);
		User retrievedUser = userManager.getUser(123);
		assertEquals(user, retrievedUser);
	}
	

	
	@Test
	public void testGetUserByTeam() {		
		List<User> users;
		users = new ArrayList<User>();
		
		User user = new User();
		user.setSid(111);
		user.setTeamId(222);
		users.add(user);
		
		user = new User();
		user.setSid(333);
		user.setTeamId(222);
		users.add(user);
		
		user = new User();
		user.setSid(444);
		user.setTeamId(555);
		users.add(user);
		
		simpleUserManager.setUsersByTeam(users);
		
		List<User> result2 = simpleUserManager.getUsersByTeam((long)111);
		assertEquals(222, result2.get(0).getTeamId());	
	}

}
