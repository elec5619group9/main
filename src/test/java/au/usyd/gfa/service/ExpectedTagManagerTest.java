package au.usyd.gfa.service;


import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import au.usyd.gfa.domain.ExpectedTag;
import au.usyd.gfa.domain.Request;

public class ExpectedTagManagerTest extends TestCase {
	
	private SimpleExpectedTagManager simpleExpectedTagManager;
	
	private List<ExpectedTag> expectedTags;
	
	protected void setUp() throws Exception {
		simpleExpectedTagManager = new SimpleExpectedTagManager();
		expectedTags = new ArrayList<ExpectedTag>();
		
		for(int i=0; i<3;i++) {
			ExpectedTag tag = new ExpectedTag();
			tag.setTeamId(1);
			tag.setTagId(i);
			expectedTags.add(tag);
			System.out.println(expectedTags.get(i).getTagId());
		}
		simpleExpectedTagManager.setTags(expectedTags);
		}
	
		public void testGetExpectedTags() {
			List<ExpectedTag> testtags = simpleExpectedTagManager.getExpectedTags((long) 1);
			for (int i = 0; i < 3; i++) {
				assertEquals(i, testtags.get(i).getTagId());
			}

		}
		
		public void testAddAndDeleteExpectedTags() {
			ExpectedTag expectedTag = new ExpectedTag();
			expectedTag.setTeamId(2);
			expectedTag.setTagId(4);
			simpleExpectedTagManager.addExpectedTag(expectedTag);
			assertEquals(1, simpleExpectedTagManager.getExpectedTags((long) 2).size());
			simpleExpectedTagManager.deleteExpectedTag(expectedTag);
			assertEquals(0, simpleExpectedTagManager.getExpectedTags((long) 2).size());
			assertEquals(3, simpleExpectedTagManager.getExpectedTags((long) 1).size());
		}
		
		public void testUpdateExpectedTags() {
			ExpectedTag expectedTag = new ExpectedTag();
			expectedTag.setTeamId(3);
			expectedTag.setTagId(5);
			simpleExpectedTagManager.updateExpectedTag(expectedTag);
			List<ExpectedTag> testtags = simpleExpectedTagManager.getExpectedTags((long)3);
			assertEquals(1, testtags.size());
			assertEquals(5, testtags.get(0).getTagId());
			
			simpleExpectedTagManager.updateExpectedTag(simpleExpectedTagManager.getExpectedTags((long)3).get(0));
			assertEquals(1, testtags.size());
			assertEquals(5, testtags.get(0).getTagId());
			

		}
			
}
