package au.usyd.gfa.service;

import static org.junit.Assert.*;


import org.junit.Test;

import au.usyd.gfa.domain.Team;


public class TeamManagerTest {

	
	private SimpleTeamManager teammanager = new SimpleTeamManager();
	@Test
	public void test() {
		Team team = new Team();
		team.setId(1);
		team.setCapacity(1);
		team.setCourseId(1);
		team.setCurrentNumber(1);
		team.setDescription("1");
		//test getGroups
		teammanager.getGroups();
		
		//test addGroup
		teammanager.addGroup(team);
		//test getGoupbyid
		Team team2 =teammanager.getGroupById(team.getId());
		assertEquals(team,team2);
		
		//test checkGroupExist
		assertEquals(true,teammanager.checkGroupExist(team.getId()));
		//test updateGroup
		team.setCapacity(2);
		teammanager.updateGroup(team);

		assertEquals(2,teammanager.getGroupById(team.getId()).getCapacity());
		//test deleteGroup
		teammanager.deleteGroup(team);
	}

}
